﻿namespace LigovkaBudget.Infrastructure.Extensions
{
    public static class MessageExtensions
    {
        public static string NotUnique(this string propertyName)
        {
            return $"{propertyName} is not unique";
        }
        public static string IsDeletedMessage(this string propertyName)
        {
            return $"{propertyName} with id {{PropertyValue}} is deleted";
        }
        public static string IsEmptyMessage(this string propertyName)
        {
            return $"Field '{propertyName}' can not be empty";
        }
        public static string ShouldBePositiveMessage(this string propertyName)
        {
            return $"The value of the field '{propertyName}' should be positive";
        }
        public static string ShouldBePositiveOrZeroMessage(this string propertyName)
        {
            return $"The value of the field '{propertyName}' should be positive or zero";
        }
        public static string CantBeMoreThanMessage(this string propertyName, int maxValue)
        {
            return $"The value of the field '{propertyName}' can not be more than {maxValue}";
        }
        public static string LengthCantBeMoreThanMessage(this string propertyName, int maxLength)
        {
            return $"The value length of the field '{propertyName}' can not be more than {maxLength}";
        }
    }
}
