﻿using System;
using System.Runtime.Serialization;

namespace LigovkaBudget.Infrastructure.Exceptions
{
    [Serializable]
    public class MethodNotAllowedException : ApplicationException
    {
        public MethodNotAllowedException() { }
        public MethodNotAllowedException(string message) : base(message) { }
        public MethodNotAllowedException(string message, Exception inner) : base(message, inner) { }
        protected MethodNotAllowedException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
