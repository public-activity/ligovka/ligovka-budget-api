﻿namespace LigovkaBudget.Infrastructure.Constraints
{
    public static class DefaultConstraints
    {
        public static readonly int DefaultMaxLength = 255;
    }
}
