﻿namespace LigovkaBudget.Infrastructure.Constraints
{
    public static class BudgetAliasConstraints
    {
        public static readonly int NameMaxLength = 1000;
    }
}
