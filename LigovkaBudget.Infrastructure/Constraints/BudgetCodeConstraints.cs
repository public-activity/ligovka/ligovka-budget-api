﻿namespace LigovkaBudget.Infrastructure.Constraints
{
    public static class BudgetCodeConstraints
    {
        public static readonly int NameMaxLength = 1000;
    }
}
