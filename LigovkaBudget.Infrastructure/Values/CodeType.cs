﻿namespace LigovkaBudget.Infrastructure.Values
{
    public enum CodeType
    {
        Income = 1,
        Expenditure
    }
}
