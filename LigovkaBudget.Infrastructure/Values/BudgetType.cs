﻿namespace LigovkaBudget.Infrastructure.Values
{
    public enum BudgetType
    {
        Project = 1,
        Decision,
        Utilization
    }
}
