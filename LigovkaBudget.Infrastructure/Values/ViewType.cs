﻿namespace LigovkaBudget.Infrastructure.Values
{
    public enum ViewType
    {
        Table = 1,
        Tree,
        Graph
    }
}
