﻿using LigovkaBudget.Core.Models.Presentations;

namespace LigovkaBudget.WebApi.InputModels.Presentations
{
    public class BudgetPresentationUpdateModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public BudgetPresentationUpdateCore ToCoreModel()
            => new BudgetPresentationUpdateCore
            {
                Id = Id,
                Name = Name
            };
    }
}
