﻿using LigovkaBudget.Core.Models.Presentations;

namespace LigovkaBudget.WebApi.InputModels.Presentations
{
    public class BudgetPresentationAddModel
    {
        public int MunicipalityId { get; set; }
        public string Name { get; set; }

        public BudgetPresentationAddCore ToCoreModel()
            => new BudgetPresentationAddCore
            {
                MunicipalityId = MunicipalityId,
                Name = Name
            };
    }
}
