﻿using LigovkaBudget.Core.Models.Details;
using LigovkaBudget.Infrastructure.Values;
using System;

namespace LigovkaBudget.WebApi.InputModels.Details
{
    public class BudgetDetailsAddModel
    {
        public int MunicipalityId { get; set; }
        public BudgetType Type { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public DateTime Date { get; set; }

        public BudgetDetailsAddCore ToCoreModel()
        {
            return new BudgetDetailsAddCore
            {
                MunicipalityId = MunicipalityId,
                Type = Type,
                Name = Name,
                Year = Year,
                Date = Date
            };
        }
    }
}
