﻿using LigovkaBudget.Core.Models.Details;
using LigovkaBudget.Infrastructure.Values;
using System;

namespace LigovkaBudget.WebApi.InputModels.Details
{
    public class BudgetDetailsUpdateModel
    {
        public int Id { get; set; }
        public BudgetType Type { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public DateTime Date { get; set; }

        public BudgetDetailsUpdateCore ToCoreModel()
        {
            return new BudgetDetailsUpdateCore
            {
                Id = Id,
                Type = Type,
                Name = Name,
                Year = Year,
                Date = Date
            };
        }
    }
}
