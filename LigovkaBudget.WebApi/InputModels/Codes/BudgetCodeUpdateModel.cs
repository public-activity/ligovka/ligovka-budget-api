﻿using LigovkaBudget.Core.Models.Codes;

namespace LigovkaBudget.WebApi.InputModels.Codes
{
    public class BudgetCodeUpdateModel
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public BudgetCodeUpdateCore ToCoreUpdateModel()
        {
            return new BudgetCodeUpdateCore
            {
                Id = Id,
                Number = Number,
                Name = Name,
                IsActive = IsActive
            };
        }
    }
}
