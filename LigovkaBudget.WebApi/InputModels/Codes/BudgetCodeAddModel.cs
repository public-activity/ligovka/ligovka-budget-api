﻿using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Infrastructure.Values;

namespace LigovkaBudget.WebApi.InputModels.Codes
{
    public class BudgetCodeAddModel
    {
        public int MunicipalityId { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public CodeType Type { get; set; }
        public bool IsActive { get; set; }

        public BudgetCodeAddCore ToCoreModel()
        {
            return new BudgetCodeAddCore
            {
                MunicipalityId = MunicipalityId,
                Number = Number,
                Name = Name,
                Type = Type,
                IsActive = IsActive
            };
        }
    }
}
