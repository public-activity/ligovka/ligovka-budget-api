﻿using LigovkaBudget.Core.Models.Aliases;
using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.WebApi.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace LigovkaBudget.WebApi.InputModels.Aliases
{
    public class BudgetAliasUpdateModel
    {
        public int Id { get; set; }
        public int? CodeId { get; set; }
        public string Name { get; set; }
        public bool IsDisplayed { get; set; }
        public IEnumerable<BudgetAliasChildRelationViewModel> ChildrenRelations { get; set; }

        public BudgetAliasUpdateCore ToCoreModel()
            => new BudgetAliasUpdateCore
            {
                Id = Id,
                Code = CodeId.HasValue ? new BudgetCodeCore { Id = CodeId.Value } : null,
                Name = Name,
                IsDisplayed = IsDisplayed,
                Relations = ChildrenRelations.Select(childRelation => new BudgetAliasRelationCore
                {
                    ChildAlias = new BudgetAliasCore { Id = childRelation.ChildId },
                    IsPrimary = childRelation.IsPrimary
                })
            };
    }
}
