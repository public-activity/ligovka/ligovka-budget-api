﻿using LigovkaBudget.Core.Models.Aliases;
using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Core.Models.Presentations;
using LigovkaBudget.Infrastructure.Values;
using LigovkaBudget.WebApi.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace LigovkaBudget.WebApi.InputModels.Aliases
{
    public class BudgetAliasAddModel
    {
        public int PresentationId { get; set; }
        public int? CodeId { get; set; }
        public CodeType CodeType { get; set; }
        public string Name { get; set; }
        public bool IsDisplayed { get; set; }
        public IEnumerable<BudgetAliasChildRelationViewModel> ChildrenRelations { get; set; }

        public BudgetAliasAddCore ToCoreModel()
            => new BudgetAliasAddCore
            {
                Presentation = new BudgetPresentationCore { Id = PresentationId },
                Code = CodeId.HasValue ? new BudgetCodeCore { Id = CodeId.Value } : null,
                CodeType = CodeType,
                Name = Name,
                IsDisplayed = IsDisplayed,
                Relations = ChildrenRelations.Select(childRelation => new BudgetAliasRelationCore
                {
                    ChildAlias = new BudgetAliasCore { Id = childRelation.ChildId },
                    IsPrimary = childRelation.IsPrimary
                })
            };
    }
}
