﻿using AutoMapper;
using LigovkaBudget.Core.Models;
using LigovkaBudget.Core.Models.Aliases;
using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Core.Models.Details;
using LigovkaBudget.Core.Models.Presentations;
using LigovkaBudget.Core.Models.Views;
using LigovkaBudget.Infrastructure.Values;
using LigovkaBudget.WebApi.ViewModels;

namespace LigovkaBudget.WebApi.Configurations
{
    public class WebApiMapperProfile : Profile
    {
        public WebApiMapperProfile()
        {
            CreateMap<BudgetType, IdWithNameViewModel>()
                .ConvertUsing(coreModel => new IdWithNameViewModel { Id = (int)coreModel, Name = coreModel.ToString() });

            CreateMap<BudgetCodeCore, BudgetCodeViewModel>();

            CreateMap<BudgetCodeWithAmountCore, BudgetCodeWithAmountViewModel>()
                .ForMember(view => view.CodeId, opt => opt.MapFrom(core => core.Code.Id))
                .ForMember(view => view.CodeNumber, opt => opt.MapFrom(core => core.Code.Number))
                .ForMember(view => view.CodeName, opt => opt.MapFrom(core => core.Code.Name))
                .ForMember(view => view.CodeType, opt => opt.MapFrom(core => core.Code.Type))
                .ForMember(view => view.CodeIsActive, opt => opt.MapFrom(core => core.Code.IsActive))
                .ForMember(view => view.Amount, opt => opt.MapFrom(core => core.Amount));

            CreateMap<BudgetDetailsCore, BudgetDetailsViewModel>();

            CreateMap<BudgetDetailsCore, BudgetDetailsWithAmountsViewModel>();

            CreateMap<BudgetAmountCore, BudgetAmountViewModel>()
                .ReverseMap();

            CreateMap<BudgetPresentationCore, BudgetPresentationViewModel>();

            CreateMap<BudgetAliasCore, BudgetAliasViewModel>()
                .ForMember(view => view.PresentationId, opt => opt.MapFrom(core => core.Presentation.Id))
                .ForMember(view => view.CodeId, opt => opt.MapFrom(core => core.Code.Id))
                .ForMember(view => view.CodeName, opt => opt.MapFrom(core => core.Code.Name))
                .ForMember(view => view.CodeNumber, opt => opt.MapFrom(core => core.Code.Number));

            CreateMap<BudgetAliasRelationCore, BudgetAliasChildRelationViewModel>()
                .ForMember(view => view.ChildId, opt => opt.MapFrom(core => core.ChildAlias.Id));

            CreateMap<BudgetAliasRelationCore, BudgetAliasParentRelationViewModel>()
                .ForMember(view => view.ParentId, opt => opt.MapFrom(core => core.ParentAlias.Id));

            CreateMap<BudgetViewCore, BudgetViewViewModel>();

            CreateMap<BudgetViewItemCore, BudgetViewItemViewModel>();

            CreateMap<BudgetViewItemRelationCore, BudgetViewItemRelationViewModel>();

            CreateMap<BudgetTypeCore, TypeViewModel>()
                .ForMember(view => view.Value, opt => opt.MapFrom(core => core.Type.ToString()));

            CreateMap<BudgetCodeTypeCore, TypeViewModel>()
                .ForMember(view => view.Value, opt => opt.MapFrom(core => core.Type.ToString()));

            CreateMap<BudgetViewTypeCore, TypeViewModel>()
                .ForMember(view => view.Value, opt => opt.MapFrom(core => core.Type.ToString()));
        }
    }
}
