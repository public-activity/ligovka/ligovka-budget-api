﻿using LigovkaBudget.Infrastructure.Values;
using System;

namespace LigovkaBudget.WebApi.ViewModels
{
    public class TypeViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
