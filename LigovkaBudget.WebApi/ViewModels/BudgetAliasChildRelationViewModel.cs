﻿namespace LigovkaBudget.WebApi.ViewModels
{
    public class BudgetAliasChildRelationViewModel : ViewModelBase
    {
        public int ChildId { get; set; }
        public bool IsPrimary { get; set; }
    }
}
