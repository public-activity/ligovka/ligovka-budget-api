﻿using LigovkaBudget.Infrastructure.Values;

namespace LigovkaBudget.WebApi.ViewModels
{
    public class BudgetCodeWithAmountViewModel : ViewModelBase
    {
        public int CodeId { get; set; }
        public string CodeNumber { get; set; }
        public string CodeName { get; set; }
        public CodeType CodeType { get; set; }
        public bool CodeIsActive { get; set; }
        public decimal? Amount { get; set; }
    }
}
