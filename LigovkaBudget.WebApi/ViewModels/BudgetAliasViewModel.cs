﻿using LigovkaBudget.Infrastructure.Values;
using System.Collections.Generic;

namespace LigovkaBudget.WebApi.ViewModels
{
    public class BudgetAliasViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public int PresentationId { get; set; }
        public int? CodeId { get; set; }
        public string CodeNumber { get; set; }
        public string CodeName { get; set; }
        public CodeType CodeType { get; set; }
        public string Name { get; set; }
        public bool IsDisplayed { get; set; }
        public IEnumerable<BudgetAliasParentRelationViewModel> ParentsRelations { get; set; }
        public IEnumerable<BudgetAliasChildRelationViewModel> ChildrenRelations { get; set; }
    }
}
