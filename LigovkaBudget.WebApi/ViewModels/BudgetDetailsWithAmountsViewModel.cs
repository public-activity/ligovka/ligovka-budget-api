﻿using LigovkaBudget.Infrastructure.Values;
using System;
using System.Collections.Generic;

namespace LigovkaBudget.WebApi.ViewModels
{
    public class BudgetDetailsWithAmountsViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public int MunicipalityId { get; set; }
        public BudgetType Type { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public DateTime Date { get; set; }
        public IEnumerable<BudgetCodeWithAmountViewModel> CodesWithAmount { get; set; }
    }
}
