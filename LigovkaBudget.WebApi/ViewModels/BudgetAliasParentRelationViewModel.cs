﻿namespace LigovkaBudget.WebApi.ViewModels
{
    public class BudgetAliasParentRelationViewModel : ViewModelBase
    {
        public int ParentId { get; set; }
        public bool IsPrimary { get; set; }
    }
}
