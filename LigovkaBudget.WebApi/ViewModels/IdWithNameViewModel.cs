﻿namespace LigovkaBudget.WebApi.ViewModels
{
    public class IdWithNameViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
