﻿namespace LigovkaBudget.WebApi.ViewModels
{
    public class BudgetPresentationViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public int MunicipalityId { get; set; }
        public string Name { get; set; }
    }
}
