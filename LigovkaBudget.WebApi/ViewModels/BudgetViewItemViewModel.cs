﻿using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Infrastructure.Values;
using System.Collections.Generic;

namespace LigovkaBudget.WebApi.ViewModels
{
    public class BudgetViewItemViewModel
    {
        public int AliasId { get; set; }
        public string AliasName { get; set; }
        public BudgetCodeCore Code { get; set; }
        public CodeType CodeType { get; set; }
        public bool IsDisplayed { get; set; }
        public decimal? CodeAmount { get; set; }
        public decimal? ChildrenAmount { get; set; }
        public IEnumerable<BudgetViewItemRelationViewModel> ParentsRelations { get; set; }
        public IEnumerable<BudgetViewItemRelationViewModel> ChildrenRelations { get; set; }
    }
}
