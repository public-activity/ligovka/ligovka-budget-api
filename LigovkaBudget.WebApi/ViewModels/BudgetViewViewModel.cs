﻿using LigovkaBudget.Infrastructure.Values;
using System;
using System.Collections.Generic;

namespace LigovkaBudget.WebApi.ViewModels
{
    public class BudgetViewViewModel : ViewModelBase
    {
        public int DetailsId { get; set; }
        public int PresentationId { get; set; }
        public int MunicipalityId { get; set; }
        public BudgetType BudgetType { get; set; }
        public string DetailsName { get; set; }
        public int DetailsYear { get; set; }
        public DateTime DetailsDate { get; set; }
        public string PresentationName { get; set; }
        public IEnumerable<BudgetViewItemViewModel> Items { get; set; }
    }
}
