﻿namespace LigovkaBudget.WebApi.ViewModels
{
    public class BudgetAmountViewModel : ViewModelBase
    {
        public int DetailsId { get; set; }
        public int CodeId { get; set; }
        public decimal Value { get; set; }
    }
}
