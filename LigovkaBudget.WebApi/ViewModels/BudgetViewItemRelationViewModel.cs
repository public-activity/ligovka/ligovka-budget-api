﻿namespace LigovkaBudget.WebApi.ViewModels
{
    public class BudgetViewItemRelationViewModel
    {
        public int ChildAliasId { get; set; }
        public int ParentAliasId { get; set; }
        public bool IsPrimary { get; set; }
    }
}
