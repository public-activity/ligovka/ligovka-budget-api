﻿using LigovkaBudget.Infrastructure.Values;
using System;

namespace LigovkaBudget.WebApi.ViewModels
{
    public class BudgetDetailsViewModel : ViewModelBase
    {
        public int Id { get; set; }
        public int MunicipalityId { get; set; }
        public BudgetType Type { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public DateTime Date { get; set; }
    }
}
