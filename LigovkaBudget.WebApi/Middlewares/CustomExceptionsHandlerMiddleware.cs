﻿using LigovkaBudget.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace LigovkaBudget.WebApi.Middlewares
{
    public class CustomExceptionsHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public CustomExceptionsHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, IWebHostEnvironment webHostEnv, ILogger<CustomExceptionsHandlerMiddleware> logger)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                logger.LogError("Exception: {0} | Message: {1} | StackTrace: {2}", exception.GetType().Name, exception.Message, exception.StackTrace);

                context.Response.ContentType = "application/json";

                switch (exception)
                {
                    case FluentValidation.ValidationException _:
                    case System.ComponentModel.DataAnnotations.ValidationException _:
                        context.Response.StatusCode = StatusCodes.Status400BadRequest;
                        await context.Response.WriteAsync(JsonSerializer.Serialize(exception.Message));
                        break;
                    case MethodNotAllowedException _:
                        context.Response.StatusCode = StatusCodes.Status405MethodNotAllowed;
                        await context.Response.WriteAsync(JsonSerializer.Serialize(exception.Message));
                        break;
                    default:
                        context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                        if (webHostEnv.IsDevelopment())
                            await context.Response.WriteAsync(exception.ToString());
                        else
                            await context.Response.WriteAsync("Internal Server Error");
                        break;
                }
            }
        }
    }
}
