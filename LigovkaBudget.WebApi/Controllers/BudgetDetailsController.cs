﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Core.Models.Details;
using LigovkaBudget.Infrastructure.Values;
using LigovkaBudget.WebApi.InputModels.Details;
using LigovkaBudget.WebApi.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.WebApi.Controllers
{
    public class BudgetDetailsController : WebApiControllerBase
    {
        private readonly IBudgetDetailsService _detailsService;
        private readonly IBudgetCodesService _codesService;

        public BudgetDetailsController(IBudgetDetailsService detailsService, IBudgetCodesService codesService, IMapper mapper)
            : base(mapper)
        {
            _detailsService = detailsService;
            _codesService = codesService;
        }

        /// <summary>
        ///     Get all budget details
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with array of budget details</response>
        [HttpGet("All")]
        [ProducesResponseType(typeof(IEnumerable<BudgetDetailsViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
            => MappedOk<IEnumerable<BudgetDetailsCore>, IEnumerable <BudgetDetailsViewModel>>(await _detailsService.GetAll());

        /// <summary>
        ///     Get budget details by municipality id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="municipalityId">id of municipality (greater than zero integer)</param>
        /// <response code="200">Successful operation with array of budget details</response>
        /// <response code="400">Incorrect input data</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<BudgetDetailsViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByMunicipalityId(int municipalityId)
            => MappedOk<IEnumerable<BudgetDetailsCore>, IEnumerable<BudgetDetailsViewModel>>(await _detailsService.Find(municipalityId));

        /// <summary>
        ///     Get budget details by id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="id">id (greater than zero integer)</param>
        /// <response code="200">Successful operation with budget details</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget details with id wasn't found</response>
        [HttpGet("{detailsId}")]
        [ProducesResponseType(typeof(BudgetDetailsViewModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetById(int detailsId)
            => MapWithCheckNotFound<BudgetDetailsCore, BudgetDetailsViewModel>(await _detailsService.FindById(detailsId));

        /// <summary>
        ///     Get budget details with amounts by id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="detailsId">id (greater than zero integer)</param>
        /// <param name="codeType">Choose type: income(1), expenditure(2). If null - will return all types</param>
        /// <param name="includeInactive">if false - only active budget codes will return</param>
        /// <response code="200">Successful operation with budget details</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget details with id wasn't found</response>
        [HttpGet("{detailsId}/Amounts")]
        [ProducesResponseType(typeof(BudgetDetailsWithAmountsViewModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIdWithAmounts(int detailsId, CodeType? codeType, bool includeInactive = false)
        {
            BudgetDetailsCore detailsCore = await _detailsService.FindById(detailsId);
            if (detailsCore == null)
                return NotFound();

            IEnumerable<BudgetCodeWithAmountCore> codesWithAmountsCore = await _codesService.FindWithAmountsByDetailsId(detailsId, codeType, includeInactive);

            var detailsView = _mapper.Map<BudgetDetailsWithAmountsViewModel>(detailsCore);
            detailsView.CodesWithAmount = _mapper.Map<IEnumerable<BudgetCodeWithAmountViewModel>>(codesWithAmountsCore);

            return Ok(detailsView);
        }

        /// <summary>
        ///     Add budget details
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with created budget details</response>
        /// <response code="400">Incorrect input data</response>
        [HttpPost]
        [ProducesResponseType(typeof(BudgetDetailsViewModel), StatusCodes.Status201Created)]
        public async Task<IActionResult> Add([FromBody] BudgetDetailsAddModel detailsAddModel)
        {
            BudgetDetailsAddCore addCoreModel = detailsAddModel.ToCoreModel();
            BudgetDetailsCore addedCoreModel = await _detailsService.Add(addCoreModel);
            return MappedOk<BudgetDetailsViewModel>(addedCoreModel);
        }

        /// <summary>
        ///     Update budget details
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with updated budget details</response>
        /// <response code="400">Incorrect input data</response>
        [HttpPut]
        [ProducesResponseType(typeof(BudgetDetailsViewModel), StatusCodes.Status201Created)]
        public async Task<IActionResult> Update([FromBody] BudgetDetailsUpdateModel detailsUpdateModel)
        {
            BudgetDetailsUpdateCore updateCoreModel = detailsUpdateModel.ToCoreModel();
            BudgetDetailsCore updatedCoreModel = await _detailsService.Update(updateCoreModel);
            return MappedOk<BudgetDetailsViewModel>(updatedCoreModel);
        }

        /// <summary>
        ///     Delete budget details by id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="204">Successful deleting operation</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget details with id wasn't found</response>
        [HttpDelete("{detailsId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Delete(int detailsId)
            => DeleteWithCheckNotFound(await _detailsService.RemoveById(detailsId));
    }
}
