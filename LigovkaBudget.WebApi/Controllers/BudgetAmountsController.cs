﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models;
using LigovkaBudget.WebApi.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.WebApi.Controllers
{
    public class BudgetAmountsController : WebApiControllerBase
    {
        private readonly IBudgetAmountsService _amountsService;

        public BudgetAmountsController(IBudgetAmountsService amountsService, IMapper mapper)
            : base(mapper)
        {
            _amountsService = amountsService;
        }

        /// <summary>
        ///     Get all budget amounts
        /// </summary>
        /// <remarks>
        ///     Available only for admin users
        /// </remarks>
        /// <response code="200">Successful operation with array of budget amounts</response>
        [HttpGet("All")]
        [ProducesResponseType(typeof(IEnumerable<BudgetAmountViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
            => MappedOk<IEnumerable<BudgetAmountCore>, IEnumerable<BudgetAmountViewModel>>(await _amountsService.GetAll());

        /// <summary>
        ///     Get budget amount by ids
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="detailsId">detailsId (greater than zero integer)</param>
        /// <param name="codeId">codeId (greater than zero integer)</param>
        /// <response code="200">Successful operation with budget amount</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget amount with id wasn't found</response>
        [HttpGet]
        [ProducesResponseType(typeof(BudgetAmountViewModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetByIds(int detailsId, int codeId)
            => MapWithCheckNotFound<BudgetAmountCore, BudgetAmountViewModel>(await _amountsService.FindByIds(detailsId, codeId));

        /// <summary>
        ///     Add budget amount
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with created budget amount</response>
        /// <response code="400">Incorrect input data</response>
        [HttpPost]
        [ProducesResponseType(typeof(BudgetAmountViewModel), StatusCodes.Status201Created)]
        public async Task<IActionResult> Add([FromBody] BudgetAmountViewModel amountAddModel)
        {
            BudgetAmountCore inputCoreModel = _mapper.Map<BudgetAmountCore>(amountAddModel);
            BudgetAmountCore addedCoreModel = await _amountsService.Add(inputCoreModel);
            return MappedOk<BudgetAmountViewModel>(addedCoreModel);
        }

        /// <summary>
        ///     Update budget amount
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with updated budget amount</response>
        /// <response code="400">Incorrect input data</response>
        [HttpPut]
        [ProducesResponseType(typeof(BudgetAmountViewModel), StatusCodes.Status201Created)]
        public async Task<IActionResult> Update([FromBody] BudgetAmountViewModel amountUpdateModel)
        {
            BudgetAmountCore inputCoreModel = _mapper.Map<BudgetAmountCore>(amountUpdateModel);
            BudgetAmountCore updatedCoreModel = await _amountsService.Update(inputCoreModel);
            return MappedOk<BudgetAmountViewModel>(updatedCoreModel);
        }

        /// <summary>
        ///     Delete budget amount by id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="detailsId">detailsId (greater than zero integer)</param>
        /// <param name="codeId">codeId (greater than zero integer)</param>
        /// <response code="204">Successful deleting operation</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget amount with id wasn't found</response>
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Delete(int detailsId, int codeId)
            => DeleteWithCheckNotFound(await _amountsService.RemoveByIds(detailsId, codeId));
    }
}
