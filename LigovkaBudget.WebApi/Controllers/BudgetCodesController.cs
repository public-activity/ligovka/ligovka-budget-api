﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Infrastructure.Values;
using LigovkaBudget.WebApi.InputModels.Codes;
using LigovkaBudget.WebApi.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.WebApi.Controllers
{
    public class BudgetCodesController : WebApiControllerBase
    {
        private readonly IBudgetCodesService _codesService;

        public BudgetCodesController(IBudgetCodesService codesService, IMapper mapper)
            : base(mapper)
        {
            _codesService = codesService;
        }

        /// <summary>
        ///     Get all budget codes
        /// </summary>
        /// <remarks>
        ///     Available for admin only
        /// </remarks>
        /// <response code="200">Successful operation with array of budget codes</response>
        [HttpGet("All")]
        [ProducesResponseType(typeof(IEnumerable<BudgetCodeViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
            => MappedOk<IEnumerable<BudgetCodeCore>, IEnumerable<BudgetCodeViewModel>>(await _codesService.GetAll());

        /// <summary>
        ///     Get array of budget codes
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="municipalityId"></param>
        /// <param name="codeType">Choose type: income(1), expenditure(2). If null - will return all types</param>
        /// <param name="includeInactive">if false - only active budget codes will return</param>
        /// <response code="200">Successful operation with array of budget codes</response>
        /// <response code="400">Incorrect input data</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<BudgetCodeViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int municipalityId, CodeType? codeType, bool includeInactive = false)
            => MappedOk<IEnumerable<BudgetCodeCore>, IEnumerable<BudgetCodeViewModel>>(await _codesService.Find(municipalityId, codeType, includeInactive));

        /// <summary>
        ///     Get budget code by id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="id">id (greater than zero integer)</param>
        /// <response code="200">Successful operation with budget code</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget code with id wasn't found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BudgetCodeViewModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetById(int id)
            => MapWithCheckNotFound<BudgetCodeCore, BudgetCodeViewModel>(await _codesService.FindById(id));

        /// <summary>
        ///     Add budget code
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with created budget code</response>
        /// <response code="400">Incorrect input data</response>
        [HttpPost]
        [ProducesResponseType(typeof(BudgetCodeViewModel), StatusCodes.Status201Created)]
        public async Task<IActionResult> Add([FromBody] BudgetCodeAddModel codeAddModel)
        {
            BudgetCodeAddCore addCoreModel = codeAddModel.ToCoreModel();
            BudgetCodeCore addedCoreModel = await _codesService.Add(addCoreModel);
            return MappedOk<BudgetCodeViewModel>(addedCoreModel);
        }

        /// <summary>
        ///     Update budget code
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with updated budget code</response>
        /// <response code="400">Incorrect input data</response>
        [HttpPut]
        [ProducesResponseType(typeof(BudgetCodeViewModel), StatusCodes.Status201Created)]
        public async Task<IActionResult> Update([FromBody] BudgetCodeUpdateModel codeUpdateModel)
        {
            BudgetCodeUpdateCore updateCoreModel = codeUpdateModel.ToCoreUpdateModel();
            BudgetCodeCore updatedCoreModel = await _codesService.Update(updateCoreModel);
            return MappedOk<BudgetCodeViewModel>(updatedCoreModel);
        }

        /// <summary>
        ///     Delete budget code by id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="204">Successful deleting operation</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget code with id wasn't found</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Delete(int id)
            => DeleteWithCheckNotFound(await _codesService.RemoveById(id));
    }
}
