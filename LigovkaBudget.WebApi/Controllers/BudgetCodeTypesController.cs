﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models;
using LigovkaBudget.WebApi.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.WebApi.Controllers
{
    public class BudgetCodeTypesController : WebApiControllerBase
    {
        private readonly IBudgetCodeTypesService _codeTypesService;

        public BudgetCodeTypesController(IBudgetCodeTypesService codeTypesService, IMapper mapper)
            : base(mapper)
        {
            _codeTypesService = codeTypesService;
        }

        /// <summary>
        ///     Get all budget code types
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with array of budget code types</response>
        [HttpGet("All")]
        [ProducesResponseType(typeof(IEnumerable<TypeViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
            => MappedOk<IEnumerable<BudgetCodeTypeCore>, IEnumerable<TypeViewModel>>(await _codeTypesService.GetAll());
    }
}
