﻿using AutoMapper;
using LigovkaBudget.Infrastructure.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace LigovkaBudget.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class WebApiControllerBase : ControllerBase
    {
        protected readonly IMapper _mapper;

        public WebApiControllerBase(IMapper mapper)
        {
            _mapper = mapper;
        }

        protected ObjectResult Created(object viewModel)
            => StatusCode(StatusCodes.Status201Created, viewModel);
        protected ObjectResult MappedCreated<TSourceModel, TViewModel>(TSourceModel sourceModel)
        {
            TViewModel viewModel = _mapper.Map<TViewModel>(sourceModel);
            return Created(viewModel);
        }
        protected ObjectResult MappedCreated<TViewModel>(object sourceModel)
        {
            TViewModel viewModel = _mapper.Map<TViewModel>(sourceModel);
            return Created(viewModel);
        }
        protected OkObjectResult MappedOk<TSourceModel, TViewModel>(TSourceModel sourceModel)
        {
            TViewModel viewModel = _mapper.Map<TViewModel>(sourceModel);
            return Ok(viewModel);
        }
        protected OkObjectResult MappedOk<TViewModel>(object sourceModel)
        {
            TViewModel viewModel = _mapper.Map<TViewModel>(sourceModel);
            return Ok(viewModel);
        }
        protected IActionResult MapWithCheckNotFound<TSourceModel, TViewModel>(object sourceModel)
            => (sourceModel == null) ? (IActionResult)NotFound() : MappedOk<TViewModel>(sourceModel);
        protected IActionResult DeleteWithCheckNotFound(bool deleteResult)
            => deleteResult ? (IActionResult)NoContent() : NotFound();
    }
}
