﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models;
using LigovkaBudget.WebApi.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.WebApi.Controllers
{
    public class BudgetViewTypesController : WebApiControllerBase
    {
        private readonly IBudgetViewTypesService _viewTypesService;

        public BudgetViewTypesController(IBudgetViewTypesService viewTypesService, IMapper mapper)
            : base(mapper)
        {
            _viewTypesService = viewTypesService;
        }

        /// <summary>
        ///     Get all budget view types
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with array of budget view types</response>
        [HttpGet("All")]
        [ProducesResponseType(typeof(IEnumerable<TypeViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
            => MappedOk<IEnumerable<BudgetViewTypeCore>, IEnumerable<TypeViewModel>>(await _viewTypesService.GetAll());
    }
}
