﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models.Aliases;
using LigovkaBudget.Infrastructure.Values;
using LigovkaBudget.WebApi.InputModels.Aliases;
using LigovkaBudget.WebApi.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.WebApi.Controllers
{
    public class BudgetAliasesController : WebApiControllerBase
    {
        private readonly IBudgetAliasesService _aliasesService;

        public BudgetAliasesController(IBudgetAliasesService aliasesService, IMapper mapper)
            : base(mapper)
        {
            _aliasesService = aliasesService;
        }

        /// <summary>
        ///     Get all budget aliases
        /// </summary>
        /// <remarks>
        ///     Available for admin only
        /// </remarks>
        /// <response code="200">Successful operation with array of budget aliases</response>
        [HttpGet("All")]
        [ProducesResponseType(typeof(IEnumerable<BudgetAliasViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
            => MappedOk<IEnumerable<BudgetAliasCore>, IEnumerable<BudgetAliasViewModel>>(await _aliasesService.GetAll());

        /// <summary>
        ///     Get array of budget aliases filtered by parameters
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="presentationId"></param>
        /// <response code="200">Successful operation with array of budget aliases</response>
        /// <response code="400">Incorrect input data</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<BudgetAliasViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int presentationId, CodeType? codeType)
            => MappedOk<IEnumerable<BudgetAliasCore>, IEnumerable<BudgetAliasViewModel>>(await _aliasesService.Find(presentationId, codeType));

        /// <summary>
        ///     Get budget alias by id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="id">id (greater than zero integer)</param>
        /// <response code="200">Successful operation with budget alias</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget alias with id wasn't found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BudgetAliasViewModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetById(int id)
            => MapWithCheckNotFound<BudgetAliasCore, BudgetAliasViewModel>(await _aliasesService.FindById(id));

        /// <summary>
        ///     Add budget alias
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with created budget alias</response>
        /// <response code="400">Incorrect input data</response>
        [HttpPost]
        [ProducesResponseType(typeof(BudgetAliasViewModel), StatusCodes.Status201Created)]
        public async Task<IActionResult> Add([FromBody] BudgetAliasAddModel aliasAddModel)
        {
            BudgetAliasAddCore inputCoreModel = aliasAddModel.ToCoreModel();
            BudgetAliasCore addedCoreModel = await _aliasesService.Add(inputCoreModel);
            return MappedOk<BudgetAliasViewModel>(addedCoreModel);
        }

        /// <summary>
        ///     Update budget alias
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with updated budget alias</response>
        /// <response code="400">Incorrect input data</response>
        [HttpPut]
        [ProducesResponseType(typeof(BudgetAliasViewModel), StatusCodes.Status201Created)]
        public async Task<IActionResult> Update([FromBody] BudgetAliasUpdateModel aliasUpdateModel)
        {
            BudgetAliasUpdateCore inputCoreModel = aliasUpdateModel.ToCoreModel();
            BudgetAliasCore updatedCoreModel = await _aliasesService.Update(inputCoreModel);
            return MappedOk<BudgetAliasViewModel>(updatedCoreModel);
        }

        /// <summary>
        ///     Delete budget alias by id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="204">Successful deleting operation</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget alias with id wasn't found</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Delete(int id)
            => DeleteWithCheckNotFound(await _aliasesService.RemoveById(id));
    }
}
