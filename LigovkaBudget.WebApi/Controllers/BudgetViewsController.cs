﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models.Views;
using LigovkaBudget.Infrastructure.Values;
using LigovkaBudget.WebApi.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LigovkaBudget.WebApi.Controllers
{
    public class BudgetViewsController : WebApiControllerBase
    {
        private readonly IBudgetViewsService _viewsService;

        public BudgetViewsController(IBudgetViewsService viewsService, IMapper mapper)
            : base(mapper)
        {
            _viewsService = viewsService;
        }

        /// <summary>
        ///     Get budget view
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="detailsId">details id (greater than zero integer)</param>
        /// <param name="presentationId">presentation id (greater than zero integer)</param>
        /// <response code="200">Successful operation with budget view</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget entities with details id and presentation id wasn't found</response>
        [HttpGet("{detailsId}/{presentationId}")]
        [ProducesResponseType(typeof(BudgetViewViewModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetById(int detailsId, int presentationId, CodeType? codeType = null)
            => MapWithCheckNotFound<BudgetViewCore, BudgetViewViewModel>(await _viewsService.Get(detailsId, presentationId, codeType));
    }
}
