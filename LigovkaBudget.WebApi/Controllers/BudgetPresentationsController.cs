﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models.Presentations;
using LigovkaBudget.Infrastructure.Values;
using LigovkaBudget.WebApi.InputModels.Presentations;
using LigovkaBudget.WebApi.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.WebApi.Controllers
{
    public class BudgetPresentationsController : WebApiControllerBase
    {
        private readonly IBudgetPresentationsService _presentationsService;

        public BudgetPresentationsController(IBudgetPresentationsService presentationsService, IMapper mapper)
            : base(mapper)
        {
            _presentationsService = presentationsService;
        }

        /// <summary>
        ///     Get all budget presentations
        /// </summary>
        /// <remarks>
        ///     Available for admin only
        /// </remarks>
        /// <response code="200">Successful operation with budget presentations</response>
        [HttpGet("All")]
        [ProducesResponseType(typeof(IEnumerable<BudgetPresentationViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
            => MappedOk<IEnumerable<BudgetPresentationCore>, IEnumerable<BudgetPresentationViewModel>>(await _presentationsService.GetAll());

        /// <summary>
        ///     Get array of budget presentations
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="municipalityId">municipality id (greater than zero integer)</param>
        /// <param name="viewType">Choose type: Table(1), Tree(2), Graph(3). If null - will return all types</param>
        /// <response code="200">Successful operation with array of budget presentations</response>
        /// <response code="400">Incorrect input data</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<BudgetPresentationViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int municipalityId, ViewType? viewType)
            => MappedOk<IEnumerable<BudgetPresentationCore>, IEnumerable<BudgetPresentationViewModel>>(await _presentationsService.Find(municipalityId, viewType));

        /// <summary>
        ///     Get budget presentation by id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="presentationId">presentation id (greater than zero integer)</param>
        /// <response code="200">Successful operation with budget presentation</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget presentation with id wasn't found</response>
        [HttpGet("{presentationId}")]
        [ProducesResponseType(typeof(BudgetPresentationViewModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetById(int presentationId)
            => MapWithCheckNotFound<BudgetPresentationCore, BudgetPresentationViewModel>(await _presentationsService.FindById(presentationId));

        /// <summary>
        ///     Get compatible view types for budget presentation by it id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="presentationId">presentation id (greater than zero integer)</param>
        /// <response code="200">Successful operation with budget presentation</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget presentation with id wasn't found</response>
        [HttpGet("{presentationId}/СompatibleViewTypes")]
        [ProducesResponseType(typeof(BudgetPresentationViewModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetViewTypesByPresentationId(int presentationId)
            => Ok(await _presentationsService.GetСompatibleViewTypes(presentationId));

        /// <summary>
        ///     Add budget presentation
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with created budget presentation</response>
        /// <response code="400">Incorrect input data</response>
        [HttpPost]
        [ProducesResponseType(typeof(BudgetPresentationViewModel), StatusCodes.Status201Created)]
        public async Task<IActionResult> Add([FromBody] BudgetPresentationAddModel presentationAddInput)
        {
            BudgetPresentationAddCore presentationAddCore = presentationAddInput.ToCoreModel();
            BudgetPresentationCore presentationAddedCore = await _presentationsService.Add(presentationAddCore);
            return MappedOk<BudgetPresentationViewModel>(presentationAddedCore);
        }

        /// <summary>
        ///     Update budget presentation
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with updated budget presentation</response>
        /// <response code="400">Incorrect input data</response>
        [HttpPut]
        [ProducesResponseType(typeof(BudgetPresentationViewModel), StatusCodes.Status201Created)]
        public async Task<IActionResult> Update([FromBody] BudgetPresentationUpdateModel presentationUpdateInput)
        {
            BudgetPresentationUpdateCore presentationUpdateCore = presentationUpdateInput.ToCoreModel();
            BudgetPresentationCore presentationUpdatedCore = await _presentationsService.Update(presentationUpdateCore);
            return MappedOk<BudgetPresentationViewModel>(presentationUpdatedCore);
        }

        /// <summary>
        ///     Delete budget presentation by id
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <param name="presentationId">presentation id (greater than zero integer)</param>
        /// <response code="204">Successful deleting operation</response>
        /// <response code="400">Incorrect input data</response>
        /// <response code="404">Budget presentation with id wasn't found</response>
        [HttpDelete("{presentationId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> Delete(int presentationId)
            => DeleteWithCheckNotFound(await _presentationsService.RemoveById(presentationId));
    }
}
