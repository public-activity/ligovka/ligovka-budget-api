﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models;
using LigovkaBudget.WebApi.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.WebApi.Controllers
{
    public class BudgetTypesController : WebApiControllerBase
    {
        private readonly IBudgetTypesService _budgetTypesService;
        public BudgetTypesController(IBudgetTypesService budgetTypesService, IMapper mapper)
            : base(mapper)
        {
            _budgetTypesService = budgetTypesService;
        }

        /// <summary>
        ///     Get all budget types
        /// </summary>
        /// <remarks>
        ///     Available for all users
        /// </remarks>
        /// <response code="200">Successful operation with array of budget types</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<TypeViewModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAll()
            => MappedOk<IEnumerable<BudgetTypeCore>, IEnumerable<TypeViewModel>>(await _budgetTypesService.GetAll());
    }
}
