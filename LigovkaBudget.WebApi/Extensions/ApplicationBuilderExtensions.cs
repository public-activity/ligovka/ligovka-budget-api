﻿using LigovkaBudget.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace LigovkaBudget.WebApi.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void UseLigovkaBudgetSwagger(this IApplicationBuilder appBuilder)
        {
            appBuilder.UseSwagger();
            appBuilder.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("v1/swagger.json", "Ligovka Budget API");
            });
        }
        public static void EnsureMigrationLigovkaBudgetContext(this IApplicationBuilder appBuilder)
        {
            using (IServiceScope scope = appBuilder.ApplicationServices.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<LigovkaBudgetContext>();
                context.Database.Migrate();
            }
        }
    }
}
