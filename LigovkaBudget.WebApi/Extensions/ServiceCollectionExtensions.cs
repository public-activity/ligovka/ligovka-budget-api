﻿using LigovkaBudget.Core.Configurations;
using LigovkaBudget.Data.Configurations;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace LigovkaBudget.WebApi.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddSwaggerGenService(this IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Ligovka Budget API", Version = "v1" });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                if (File.Exists(xmlPath))
                    options.IncludeXmlComments(xmlPath);
            });
        }
        public static void AddWebApiServices(this IServiceCollection services)
        { }
        public static void AddCoreServices(this IServiceCollection services)
            => CoreServices.AddCoreServices(services);
        public static void AddDataServices(this IServiceCollection services, string connectionString)
            => DataServices.AddDataServices(services, connectionString);
    }
}
