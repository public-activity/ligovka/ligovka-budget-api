using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using NLog;
using NLog.Config;
using NLog.Extensions.Logging;
using NLog.Web;
using System;
using System.IO;

namespace LigovkaBudget.WebApi
{
    public class EntryPoint
    {
        public static void Main(string[] args)
        {
            try
            {
                ConfigureNlog();

                IHostBuilder hostBuilder = Host.CreateDefaultBuilder(args)
                    .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>())
                    .UseNLog();

                IHost host = hostBuilder.Build();
                host.Run();
            }
            catch (Exception exception)
            {
                Logger logger = LogManager.GetCurrentClassLogger();
                logger?.Fatal(exception, "Fatal exception during host run");
            }
        }

        private static void ConfigureNlog()
        {
            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            LoggingConfiguration loggingConfig = new NLogLoggingConfiguration(configuration.GetSection("NLog"));
            NLogBuilder.ConfigureNLog(loggingConfig);
        }
    }
}
