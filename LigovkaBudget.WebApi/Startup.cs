using AutoMapper;
using LigovkaBudget.Core.Configurations;
using LigovkaBudget.Data;
using LigovkaBudget.WebApi.Configurations;
using LigovkaBudget.WebApi.Extensions;
using LigovkaBudget.WebApi.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Text.Json.Serialization;

namespace LigovkaBudget.WebApi
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(config =>
            {
                config.AddProfile<WebApiMapperProfile>();
                config.AddProfile<CoreMapperProfile>();
            });
            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });
            services.AddCors();
            services.AddSwaggerGenService();
            services.AddWebApiServices();
            services.AddCoreServices();
            services.AddDataServices(_configuration.GetConnectionString("LigovkaBudgetConnection"));
        }
        public void Configure(IApplicationBuilder appBuilder, ILogger<Startup> logger, LigovkaBudgetContext context)
        {
            logger.LogDebug("Startup configuring");

            appBuilder.UseMiddleware<CustomExceptionsHandlerMiddleware>();
            appBuilder.EnsureMigrationLigovkaBudgetContext();
            appBuilder.UseStaticFiles();
            appBuilder.UseRouting();
            appBuilder.UseCors(builder => builder
                .WithOrigins(_configuration.GetValue<string>("AllowedHosts"))
                .AllowAnyHeader()
                .AllowAnyMethod()
            );
            appBuilder.UseAuthorization();
            appBuilder.UseLigovkaBudgetSwagger();
            appBuilder.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Web API is now running.");
                });
            });
        }
    }
}
