﻿using AutoMapper;
using LigovkaBudget.Core.Configurations;
using LigovkaBudget.Core.Models;
using LigovkaBudget.Core.Services;
using LigovkaBudget.Data;
using LigovkaBudget.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LigovkaBudget.Tests.LigovkaBudget.Core.Services
{
    [TestClass]
    public class BudgetAmountsServiceTest
    {
        private readonly LigovkaBudgetContext _ligovkaBudgetContext;
        private readonly BudgetAmountsService _budgetAmountsService;

        private readonly IEnumerable<BudgetAmountData> _defaultBudgetAmountExamples = new BudgetAmountData[]
        {
            new BudgetAmountData { CodeId = 1, DetailsId = 1, Value = 1 },
            new BudgetAmountData { CodeId = 2, DetailsId = 1, Value = 10 },
            new BudgetAmountData { CodeId = 1, DetailsId = 2, Value = 100 },
        };

        public BudgetAmountsServiceTest()
        {
            DbContextOptions<LigovkaBudgetContext> contextOptions = new DbContextOptionsBuilder<LigovkaBudgetContext>()
                .UseInMemoryDatabase("BudgetValuesServiceTest")
                .Options;
            _ligovkaBudgetContext = new LigovkaBudgetContext(contextOptions);

            var mappingConfig = new MapperConfiguration(cfg => cfg.AddProfile<CoreMapperProfile>());
            IMapper mapper = mappingConfig.CreateMapper();

            _budgetAmountsService = new BudgetAmountsService(_ligovkaBudgetContext, mapper);
        }

        [TestMethod]
        public async Task GetAll_Count()
        {
            await SetExamplesToContext(_defaultBudgetAmountExamples);

            IEnumerable<BudgetAmountCore> result = await _budgetAmountsService.GetAll();

            Assert.AreEqual(_defaultBudgetAmountExamples.Count(), result.Count());
        }

        [TestMethod]
        public async Task RemoveByIdAsync_ExistingIds_ReturnsTrue()
        {
            await SetExamplesToContext(_defaultBudgetAmountExamples);

            bool result = await _budgetAmountsService.RemoveByIds(1, 2);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task RemoveByIdAsync_NotExistingIds_ReturnsFalse()
        {
            await SetExamplesToContext(_defaultBudgetAmountExamples);

            bool result = await _budgetAmountsService.RemoveByIds(100, 100);

            Assert.IsFalse(result);
        }

        private async Task SetExamplesToContext(IEnumerable<BudgetAmountData> budgetAmountExamples)
        {
            _ligovkaBudgetContext.Database.EnsureDeleted();
            _ligovkaBudgetContext.Amounts.AddRange(budgetAmountExamples);
            await _ligovkaBudgetContext.SaveChangesAsync();
        }
    }
}
