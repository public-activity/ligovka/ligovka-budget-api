﻿using LigovkaBudget.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Interfaces
{
    public interface IBudgetTypesService
    {
        Task<IEnumerable<BudgetTypeCore>> GetAll();
    }
}
