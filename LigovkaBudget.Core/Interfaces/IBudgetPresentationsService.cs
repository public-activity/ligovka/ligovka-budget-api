﻿using LigovkaBudget.Core.Models.Presentations;
using LigovkaBudget.Infrastructure.Values;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Interfaces
{
    public interface IBudgetPresentationsService
    {
        Task<IEnumerable<BudgetPresentationCore>> GetAll();
        Task<IEnumerable<BudgetPresentationCore>> Find(int municipalityId, ViewType? viewType);
        Task<BudgetPresentationCore> FindById(int presentationId);
        Task<BudgetPresentationCore> Add(BudgetPresentationAddCore presentationAddCore);
        Task<IEnumerable<BudgetPresentationCore>> Add(IEnumerable<BudgetPresentationAddCore> presentationAddCores);
        Task<BudgetPresentationCore> Update(BudgetPresentationUpdateCore presentationUpdateCore);
        Task<bool> RemoveById(int presentationId);
        Task<IEnumerable<ViewType>> GetСompatibleViewTypes(int presentationId);
    }
}

