﻿using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Infrastructure.Values;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Interfaces
{
    public interface IBudgetCodesService
    {
        Task<IEnumerable<BudgetCodeCore>> GetAll();
        Task<IEnumerable<BudgetCodeCore>> Find(int municipalityId, CodeType? codeType, bool includeInactive);
        Task<IEnumerable<BudgetCodeWithAmountCore>> FindWithAmountsByDetailsId(int detailsId, CodeType? codeType, bool includeInactive);
        Task<BudgetCodeCore> FindById(int codeId);
        Task<BudgetCodeCore> Add(BudgetCodeAddCore codeAdd);
        Task<IEnumerable<BudgetCodeCore>> Add(IEnumerable<BudgetCodeAddCore> codeAddes);
        Task<BudgetCodeCore> Update(BudgetCodeUpdateCore codeUpdate);
        Task<IEnumerable<BudgetCodeCore>> Update(IEnumerable<BudgetCodeUpdateCore> codeUpdates);
        Task<bool> RemoveById(int codeId);
    }
}
