﻿using LigovkaBudget.Core.Models.Aliases;
using LigovkaBudget.Infrastructure.Values;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Interfaces
{
    public interface IBudgetAliasesService
    {
        Task<IEnumerable<BudgetAliasCore>> GetAll();
        Task<IEnumerable<BudgetAliasCore>> Find(int presentationId, CodeType? codeType);
        Task<BudgetAliasCore> FindById(int aliasId);
        Task<BudgetAliasCore> Add(BudgetAliasAddCore aliasAdd);
        Task<IEnumerable<BudgetAliasCore>> Add(IEnumerable<BudgetAliasAddCore> aliasAddes);
        Task<BudgetAliasCore> Update(BudgetAliasUpdateCore aliasUpdate);
        Task<bool> RemoveById(int aliasId);
    }
}
