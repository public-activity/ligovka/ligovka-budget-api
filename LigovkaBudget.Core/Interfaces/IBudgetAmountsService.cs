﻿using LigovkaBudget.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Interfaces
{
    public interface IBudgetAmountsService
    {
        Task<IEnumerable<BudgetAmountCore>> GetAll();
        Task<BudgetAmountCore> FindByIds(int detailsId, int codeId);
        Task<BudgetAmountCore> Add(BudgetAmountCore inputAmount);
        Task<IEnumerable<BudgetAmountCore>> Add(IEnumerable<BudgetAmountCore> inputAmountes);
        Task<BudgetAmountCore> Update(BudgetAmountCore inputAmount);
        Task<bool> RemoveByIds(int detailsId, int codeId);
    }
}
