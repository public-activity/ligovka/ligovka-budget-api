﻿using LigovkaBudget.Core.Models.Views;
using LigovkaBudget.Infrastructure.Values;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Interfaces
{
    public interface IBudgetViewsService
    {
        Task<BudgetViewCore> Get(int detailsId, int presentationId, CodeType? codeType = null);
    }
}
