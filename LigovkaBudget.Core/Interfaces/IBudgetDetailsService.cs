﻿using LigovkaBudget.Core.Models.Details;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Interfaces
{
    public interface IBudgetDetailsService
    {
        Task<IEnumerable<BudgetDetailsCore>> GetAll();
        Task<IEnumerable<BudgetDetailsCore>> Find(int municipalityId);
        Task<BudgetDetailsCore> FindById(int detailsId);
        Task<BudgetDetailsCore> Add(BudgetDetailsAddCore detailsAdd);
        Task<IEnumerable<BudgetDetailsCore>> Add(IEnumerable<BudgetDetailsAddCore> detailsAddes);
        Task<BudgetDetailsCore> Update(BudgetDetailsUpdateCore detailsUpdate);
        Task<bool> RemoveById(int detailsId);
    }
}
