﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models;
using LigovkaBudget.Data;
using LigovkaBudget.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Services
{
    public class BudgetTypesService : BudgetServiceBase, IBudgetTypesService
    {
        public BudgetTypesService(LigovkaBudgetContext context, IMapper mapper)
            : base(context, mapper)
        { }

        public async Task<IEnumerable<BudgetTypeCore>> GetAll()
        {
            IEnumerable<BudgetTypeData> budgetTypesDatas = await _context.BudgetTypes.AsNoTracking().ToArrayAsync();
            var budgetTypesCores = _mapper.Map<IEnumerable<BudgetTypeCore>>(budgetTypesDatas);
            return budgetTypesCores;
        }
    }
}
