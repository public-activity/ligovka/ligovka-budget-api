﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models;
using LigovkaBudget.Data;
using LigovkaBudget.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Services
{
    public sealed class BudgetAmountsService : BudgetServiceCrudBase<BudgetAmountData, BudgetAmountCore>, IBudgetAmountsService
    {
        public BudgetAmountsService(LigovkaBudgetContext context, IMapper mapper)
            : base(context, mapper)
        { }

        public new Task<IEnumerable<BudgetAmountCore>> GetAll()
            => base.GetAll();
        public Task<BudgetAmountCore> FindByIds(int detailsId, int codeId)
            => FindOne(amount => amount.DetailsId == detailsId && amount.CodeId == codeId);
        public Task<BudgetAmountCore> Add(BudgetAmountCore inputAmount)
            => AddOne(inputAmount);
        public Task<IEnumerable<BudgetAmountCore>> Add(IEnumerable<BudgetAmountCore> inputAmounts)
            => AddMany(inputAmounts);
        public Task<BudgetAmountCore> Update(BudgetAmountCore inputAmount)
            => UpdateOne(amount => amount.DetailsId == inputAmount.Details.Id && amount.CodeId == inputAmount.Code.Id,
                amount => amount.Value = inputAmount.Value);
        public Task<bool> RemoveByIds(int detailsId, int codeId)
            => Remove(amount => amount.DetailsId == detailsId && amount.CodeId == codeId);
    }
}
