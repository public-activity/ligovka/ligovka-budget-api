﻿using AutoMapper;
using LigovkaBudget.Data;

namespace LigovkaBudget.Core.Services
{
    public abstract class BudgetServiceBase
    {
        protected readonly LigovkaBudgetContext _context;
        protected readonly IMapper _mapper;

        protected BudgetServiceBase(LigovkaBudgetContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
