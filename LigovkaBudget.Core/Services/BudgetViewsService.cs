﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Core.Models.Views;
using LigovkaBudget.Data;
using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Services
{
    public class BudgetViewsService : BudgetServiceBase, IBudgetViewsService
    {
        public BudgetViewsService(LigovkaBudgetContext context, IMapper mapper)
            : base(context, mapper)
        { }

        public async Task<BudgetViewCore> Get(int detailsId, int presentationId, CodeType? codeType = null)
        {
            var details = await _context.Details.AsNoTracking().FirstOrDefaultAsync(detailsData => detailsData.Id == detailsId);
            if (details == null)
                return null;

            var presentation = await _context.Presentations.AsNoTracking().FirstOrDefaultAsync(presentationData => presentationData.Id == presentationId);
            if (presentation == null)
                return null;

            if (details.MunicipalityId != presentation.MunicipalityId)
                return null; //Validation error in future

            var requestDatas = await _context.Aliases
                .Include(alias => alias.Code)
                .Include(alias => alias.ChildrenRelations)
                .Include(alias => alias.ParentsRelations)
                .AsNoTracking()
                .Where(aliasData => aliasData.PresentationId == presentationId
                    && (!codeType.HasValue || aliasData.CodeType == codeType.Value))
                .SelectMany(aliasData => _context.Amounts
                        .Where(amountData => amountData.CodeId == aliasData.CodeId
                            && amountData.DetailsId == detailsId)
                        .DefaultIfEmpty(),
                    (aliasData, amountValue) => new { Alias = aliasData, Amount = amountValue != null ? amountValue.Value : (decimal?)null })
                .ToArrayAsync();

            BudgetViewItemCore[] items = requestDatas
                .Select(data => new BudgetViewItemCore
                {
                    AliasId = data.Alias.Id,
                    AliasName = data.Alias.Name,
                    Code = _mapper.Map<BudgetCodeCore>(data.Alias.Code),
                    CodeType = data.Alias.CodeType,
                    CodeAmount = data.Amount,
                    IsDisplayed = data.Alias.IsDisplayed,
                    ParentsRelations = _mapper.Map<IEnumerable<BudgetViewItemRelationCore>>(data.Alias.ParentsRelations),
                    ChildrenRelations = _mapper.Map<IEnumerable<BudgetViewItemRelationCore>>(data.Alias.ChildrenRelations)
                })
                .ToArray();

            CalculateChildrenAmounts(items);

            var budgetView = new BudgetViewCore
            {
                DetailsId = details.Id,
                MunicipalityId = details.MunicipalityId,
                BudgetType = details.Type,
                DetailsName = details.Name,
                DetailsYear = details.Year,
                DetailsDate = details.Date,
                PresentationId = presentation.Id,
                PresentationName = presentation.Name,
                Items = items
            };

            return budgetView;
        }

        private void CalculateChildrenAmounts(BudgetViewItemCore[] items)
        {
            var itemsForCalculate = items
                .Select(item => new ItemForCalculate { Item = item, IsCalculated = false })
                .ToArray();

            while (itemsForCalculate.Any(item => !item.IsCalculated))
            {
                ItemForCalculate itemForCalculate = itemsForCalculate.First(item => !item.IsCalculated);
                CalculateItemAmount(itemsForCalculate, itemForCalculate);
            }
        }
        private void CalculateItemAmount(ItemForCalculate[] allItems, ItemForCalculate itemForCalculate)
        {
            if (itemForCalculate.Item.ChildrenRelations.Any())
            {
                var children = allItems
                    .Where(item => itemForCalculate.Item.ChildrenRelations.Any(relation => relation.ChildAliasId == item.Item.AliasId));

                foreach(var child in children.Where(item => !item.IsCalculated))
                    CalculateItemAmount(allItems, child);

                itemForCalculate.Item.ChildrenAmount = children.Sum(item => item.Item.ChildrenAmount);
            }
            else
            {
                itemForCalculate.Item.ChildrenAmount = itemForCalculate.Item.CodeAmount;
            }

            itemForCalculate.IsCalculated = true;
        }

        private class ItemForCalculate
        {
            public BudgetViewItemCore Item { get; set; }
            public bool IsCalculated { get; set; }
        }
    }
}
