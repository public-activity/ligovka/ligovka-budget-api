﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Data;
using LigovkaBudget.Data.Models;
using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Services
{
    public class BudgetCodesService : BudgetServiceCrudBase<BudgetCodeData, BudgetCodeCore>, IBudgetCodesService
    {
        public BudgetCodesService(LigovkaBudgetContext context, IMapper mapper)
            : base(context, mapper)
        { }

        public new Task<IEnumerable<BudgetCodeCore>> GetAll()
            => base.GetAll();
        public Task<IEnumerable<BudgetCodeCore>> Find(int municipalityId, CodeType? codeType, bool includeInactive)
            => FindMany(codeData => codeData.MunicipalityId == municipalityId
                && (!codeType.HasValue || codeData.Type == codeType.Value)
                && (includeInactive || codeData.IsActive));
        public async Task<IEnumerable<BudgetCodeWithAmountCore>> FindWithAmountsByDetailsId(int detailsId, CodeType? codeType, bool includeInactive)
        {
            var requestDatas = await _context.Codes
                .AsNoTracking()
                .Where(codeData => (!codeType.HasValue || codeData.Type == codeType.Value)
                    && (includeInactive || codeData.IsActive))
                .SelectMany(codeData => _context.Amounts
                        .Where(amountData => amountData.CodeId == codeData.Id 
                            && amountData.DetailsId == detailsId)
                        .DefaultIfEmpty(),
                    (codeData, amountValue) => new { Code = codeData, Amount = amountValue != null ? amountValue.Value : (decimal?)null })
                .ToArrayAsync();

            BudgetCodeWithAmountCore[] codesWithAmounts = requestDatas
                .Select(data => new BudgetCodeWithAmountCore { Code = _mapper.Map<BudgetCodeCore>(data.Code), Amount = data.Amount })
                .ToArray();

            return codesWithAmounts;
        }
        public Task<BudgetCodeCore> FindById(int codeId)
            => FindOne(codeData => codeData.Id == codeId);
        public Task<BudgetCodeCore> Add(BudgetCodeAddCore codeAdd)
            => AddOne(new BudgetCodeCore
                {
                    MunicipalityId = codeAdd.MunicipalityId,
                    Number = codeAdd.Number,
                    Name = codeAdd.Name,
                    Type = codeAdd.Type,
                    IsActive = codeAdd.IsActive,
                    CreatedAt = DateTime.UtcNow
                });
        public Task<IEnumerable<BudgetCodeCore>> Add(IEnumerable<BudgetCodeAddCore> codeAddes)
            => AddMany(codeAddes.Select(codeAdd => new BudgetCodeCore
                {
                    MunicipalityId = codeAdd.MunicipalityId,
                    Number = codeAdd.Number,
                    Name = codeAdd.Name,
                    Type = codeAdd.Type,
                    IsActive = codeAdd.IsActive,
                    CreatedAt = DateTime.UtcNow
                }));
        public Task<BudgetCodeCore> Update(BudgetCodeUpdateCore codeUpdate)
            => UpdateOne(codeData => codeData.Id == codeUpdate.Id, codeData =>
                {
                    codeData.Number = codeUpdate.Number;
                    codeData.Name = codeUpdate.Name;
                    codeData.IsActive = codeUpdate.IsActive;
                    codeData.UpdatedAt = DateTime.UtcNow;
                });
        public async Task<IEnumerable<BudgetCodeCore>> Update(IEnumerable<BudgetCodeUpdateCore> codeUpdates)
        {
            IList<BudgetCodeData> codeDatas = new List<BudgetCodeData>();

            foreach (var codeUpdate in codeUpdates)
            {
                BudgetCodeData codeData = await _dbSet.FirstOrDefaultAsync(code => code.Id == codeUpdate.Id);

                codeData.Number = codeUpdate.Number;
                codeData.Name = codeUpdate.Name;
                codeData.IsActive = codeUpdate.IsActive;
                codeData.UpdatedAt = DateTime.UtcNow;
                _dbSet.Update(codeData);

                codeDatas.Add(codeData);
            }

            await _context.SaveChangesAsync();

            var updatedCoreModels = _mapper.Map<IEnumerable<BudgetCodeCore>>(codeDatas);
            return updatedCoreModels;
        }
        public Task<bool> RemoveById(int codeId)
            => Remove(codeData => codeData.Id == codeId);
    }
}
