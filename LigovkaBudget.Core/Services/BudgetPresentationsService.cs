﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models.Presentations;
using LigovkaBudget.Data;
using LigovkaBudget.Data.Models;
using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Services
{
    public class BudgetPresentationsService : BudgetServiceCrudBase<BudgetPresentationData, BudgetPresentationCore>, IBudgetPresentationsService
    {
        public BudgetPresentationsService(LigovkaBudgetContext context, IMapper mapper)
            : base(context, mapper)
        { }

        public Task<IEnumerable<BudgetPresentationCore>> GetAll()
            => base.GetAll();
        public async Task<IEnumerable<BudgetPresentationCore>> Find(int municipalityId, ViewType? viewType)
        {
            if (!viewType.HasValue)
                return await FindMany(presentationData => presentationData.MunicipalityId == municipalityId);
            else
            {
                IEnumerable<BudgetPresentationData> presentationsData = await _context.PresentationsViewTypes
                    .Include(pvt => pvt.Presentation)
                    .Where(pvt => pvt.Presentation.MunicipalityId == municipalityId
                        && pvt.ViewType == viewType)
                    .Select(pvt => pvt.Presentation)
                    .ToArrayAsync();

                var presentationsCore = _mapper.Map<IEnumerable<BudgetPresentationCore>>(presentationsData);
                return presentationsCore;
            }
        }
        public Task<BudgetPresentationCore> FindById(int presentationId)
            => FindOne(presentationData => presentationData.Id == presentationId);
        public Task<BudgetPresentationCore> Add(BudgetPresentationAddCore presentationAddCore)
            => AddOne(new BudgetPresentationCore
            {
                MunicipalityId = presentationAddCore.MunicipalityId,
                Name = presentationAddCore.Name
            });
        public Task<IEnumerable<BudgetPresentationCore>> Add(IEnumerable<BudgetPresentationAddCore> presentationAddCores)
            => AddMany(presentationAddCores.Select(presentationAddCore => new BudgetPresentationCore
            {
                MunicipalityId = presentationAddCore.MunicipalityId,
                Name = presentationAddCore.Name
            }));
        public Task<BudgetPresentationCore> Update(BudgetPresentationUpdateCore presentationUpdateCore)
            => UpdateOne(presentationData => presentationData.Id == presentationUpdateCore.Id, presentationData =>
            {
                presentationData.Name = presentationUpdateCore.Name;
            });
        public Task<bool> RemoveById(int presentationId)
            => Remove(presentationData => presentationData.Id == presentationId);
        public async Task<IEnumerable<ViewType>> GetСompatibleViewTypes(int presentationId)
            => await _context.PresentationsViewTypes
                .Where(pvt => pvt.PresentationId == presentationId)
                .Select(pvt => pvt.ViewType)
                .ToArrayAsync();
    }
}
