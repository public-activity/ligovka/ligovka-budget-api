﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models;
using LigovkaBudget.Data;
using LigovkaBudget.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Services
{
    public class BudgetViewTypesService : BudgetServiceBase, IBudgetViewTypesService
    {
        public BudgetViewTypesService(LigovkaBudgetContext context, IMapper mapper)
            : base(context, mapper)
        { }

        public async Task<IEnumerable<BudgetViewTypeCore>> GetAll()
        {
            IEnumerable<BudgetViewTypeData> viewTypesDatas = await _context.ViewTypes.AsNoTracking().ToArrayAsync();
            var viewTypesCores = _mapper.Map<IEnumerable<BudgetViewTypeCore>>(viewTypesDatas);
            return viewTypesCores;
        }
    }
}
