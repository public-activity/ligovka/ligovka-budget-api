﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models;
using LigovkaBudget.Data;
using LigovkaBudget.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Services
{
    public class BudgetCodeTypesService : BudgetServiceBase, IBudgetCodeTypesService
    {
        public BudgetCodeTypesService(LigovkaBudgetContext context, IMapper mapper)
            : base(context, mapper)
        { }

        public async Task<IEnumerable<BudgetCodeTypeCore>> GetAll()
        {
            IEnumerable<BudgetCodeTypeData> codeTypesDatas = await _context.CodeTypes.AsNoTracking().ToArrayAsync();
            var codeTypesCores = _mapper.Map<IEnumerable<BudgetCodeTypeCore>>(codeTypesDatas);
            return codeTypesCores;
        }
    }
}
