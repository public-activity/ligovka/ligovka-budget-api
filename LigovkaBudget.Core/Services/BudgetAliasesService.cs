﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models.Aliases;
using LigovkaBudget.Data;
using LigovkaBudget.Data.Models;
using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Services
{
    public sealed class BudgetAliasesService : BudgetServiceCrudBase<BudgetAliasData, BudgetAliasCore>, IBudgetAliasesService
    {
        private readonly string[] _defaultIncludeFields = new[]
        {
            nameof(BudgetAliasData.Code),
            nameof(BudgetAliasData.ChildrenRelations),
            nameof(BudgetAliasData.ParentsRelations)
        };

        public BudgetAliasesService(LigovkaBudgetContext context, IMapper mapper)
            : base(context, mapper)
        { }

        public Task<IEnumerable<BudgetAliasCore>> GetAll()
            => GetAll(_defaultIncludeFields);
        public Task<IEnumerable<BudgetAliasCore>> Find(int presentationId, CodeType? codeType)
            => FindMany(aliasData => aliasData.PresentationId == presentationId && (!codeType.HasValue || aliasData.CodeType == codeType.Value),
                _defaultIncludeFields);
        public Task<BudgetAliasCore> FindById(int aliasId)
            => FindOne(aliasData => aliasData.Id == aliasId,
                _defaultIncludeFields);
        public async Task<BudgetAliasCore> Add(BudgetAliasAddCore aliasAdd)
        {
            BudgetAliasData aliasAddedData = await AddAliasWithRelations(aliasAdd);

            await _context.SaveChangesAsync();
            await LoadReferences(aliasAddedData, nameof(BudgetAliasData.Code));
            await LoadCollections(aliasAddedData, nameof(BudgetAliasData.ParentsRelations));

            var aliasAddedCore = _mapper.Map<BudgetAliasCore>(aliasAddedData);
            return aliasAddedCore;
        }
        public async Task<IEnumerable<BudgetAliasCore>> Add(IEnumerable<BudgetAliasAddCore> aliasAddes)
        {
            var aliasAddedData = new BudgetAliasData[aliasAddes.Count()];

            int index = 0;
            foreach (BudgetAliasAddCore aliasAdd in aliasAddes)
                aliasAddedData[index++] = await AddAliasWithRelations(aliasAdd);

            await _context.SaveChangesAsync();

            foreach (BudgetAliasData aliasAddData in aliasAddedData)
            {
                await LoadReferences(aliasAddData, nameof(BudgetAliasData.Code));
                await LoadCollections(aliasAddData, nameof(BudgetAliasData.ParentsRelations));
            }

            var aliasAddedCore = _mapper.Map<IEnumerable<BudgetAliasCore>>(aliasAddedData);
            return aliasAddedCore;
        }
        public async Task<BudgetAliasCore> Update(BudgetAliasUpdateCore aliasUpdate)
        {
            BudgetAliasData aliasData = await _dbSet
                .Include(aliasData => aliasData.ChildrenRelations)
                .FirstOrDefaultAsync(aliasData => aliasData.Id == aliasUpdate.Id);

            var relationsForAdd = aliasUpdate.Relations
                .Where(relationUpdate => !aliasData.ChildrenRelations.Any(relationData => relationData.ChildAliasId == relationUpdate.ChildAlias.Id))
                .ToArray();
            var relationsForUpdateData = aliasData.ChildrenRelations
                .Where(relationData => aliasUpdate.Relations.Any(relationUpdate => relationData.ChildAliasId == relationUpdate.ChildAlias.Id))
                .ToArray();
            var relationsForRemoveData = aliasData.ChildrenRelations
                .Where(relationData => !aliasUpdate.Relations.Any(relationUpdate => relationData.ChildAliasId == relationUpdate.ChildAlias.Id))
                .ToArray();

            await AddAliasRelations(aliasData, relationsForAdd);

            foreach (var relationForUpdateData in relationsForUpdateData)
            {
                bool isPrimaryUpdate = aliasUpdate.Relations.FirstOrDefault(relation => relation.ChildAlias.Id == relationForUpdateData.ChildAliasId).IsPrimary;
                if (relationForUpdateData.IsPrimary != isPrimaryUpdate)
                {
                    relationForUpdateData.IsPrimary = isPrimaryUpdate;
                    _context.AliasRelations.Update(relationForUpdateData);
                }
            }

            _context.AliasRelations.RemoveRange(relationsForRemoveData);

            aliasData.CodeId = aliasUpdate.Code?.Id;
            aliasData.Name = aliasUpdate.Name;
            aliasData.IsDisplayed = aliasUpdate.IsDisplayed;
            _dbSet.Update(aliasData);

            await _context.SaveChangesAsync();

            await LoadReferences(aliasData, nameof(BudgetAliasCore.Code));
            await LoadCollections(aliasData, nameof(BudgetAliasData.ParentsRelations));

            var aliasUpdatedCore = _mapper.Map<BudgetAliasCore>(aliasData);
            return aliasUpdatedCore;
        }
        public Task<bool> RemoveById(int aliasId)
            => Remove(alias => alias.Id == aliasId);

        private async Task<BudgetAliasData> AddAliasWithRelations(BudgetAliasAddCore aliasAdd)
        {
            var aliasData = new BudgetAliasData
            {
                PresentationId = aliasAdd.Presentation.Id,
                CodeId = aliasAdd.Code?.Id,
                CodeType = aliasAdd.CodeType,
                Name = aliasAdd.Name,
                IsDisplayed = aliasAdd.IsDisplayed
            };
            await _dbSet.AddAsync(aliasData);
            await AddAliasRelations(aliasData, aliasAdd.Relations);
            return aliasData;
        }
        private async Task AddAliasRelations(BudgetAliasData aliasData, IEnumerable<BudgetAliasRelationCore> aliasRelationsAdd)
        {
            foreach(var relationAdd in aliasRelationsAdd)
            {
                await _context.AliasRelations.AddAsync(new BudgetAliasRelationData
                {
                    ChildAliasId = relationAdd.ChildAlias.Id,
                    ParentAlias = aliasData,
                    IsPrimary = relationAdd.IsPrimary
                });
            }
        }
    }
}
