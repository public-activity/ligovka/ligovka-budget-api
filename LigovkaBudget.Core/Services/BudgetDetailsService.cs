﻿using AutoMapper;
using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Models.Details;
using LigovkaBudget.Data;
using LigovkaBudget.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Services
{
    public class BudgetDetailsService : BudgetServiceCrudBase<BudgetDetailsData, BudgetDetailsCore>, IBudgetDetailsService
    {
        public BudgetDetailsService(LigovkaBudgetContext context, IMapper mapper)
            : base(context, mapper)
        { }

        public new Task<IEnumerable<BudgetDetailsCore>> GetAll()
            => base.GetAll();
        public Task<IEnumerable<BudgetDetailsCore>> Find(int municipalityId)
            => FindMany(detailsData => detailsData.MunicipalityId == municipalityId);
        public Task<BudgetDetailsCore> FindById(int id)
            => FindOne(detailsData => detailsData.Id == id);
        public Task<BudgetDetailsCore> Add(BudgetDetailsAddCore detailsAdd)
            => AddOne(new BudgetDetailsCore
            {
                MunicipalityId = detailsAdd.MunicipalityId,
                Type = detailsAdd.Type,
                Name = detailsAdd.Name,
                Year = detailsAdd.Year,
                CreatedAt = DateTime.UtcNow
            });
        public Task<IEnumerable<BudgetDetailsCore>> Add(IEnumerable<BudgetDetailsAddCore> detailsAddes)
            => AddMany(detailsAddes.Select(detailsAdd => new BudgetDetailsCore
            {
                MunicipalityId = detailsAdd.MunicipalityId,
                Type = detailsAdd.Type,
                Name = detailsAdd.Name,
                Year = detailsAdd.Year,
                CreatedAt = DateTime.UtcNow
            }));
        public Task<BudgetDetailsCore> Update(BudgetDetailsUpdateCore detailsUpdate)
            => UpdateOne(details => details.Id == detailsUpdate.Id, details =>
            {
                details.Type = detailsUpdate.Type;
                details.Name = detailsUpdate.Name;
                details.Year = detailsUpdate.Year;
                details.Date = detailsUpdate.Date;
            });
        public Task<bool> RemoveById(int id)
            => Remove(details => details.Id == id);
    }
}
