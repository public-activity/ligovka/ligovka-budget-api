﻿using AutoMapper;
using LigovkaBudget.Core.Models;
using LigovkaBudget.Data;
using LigovkaBudget.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LigovkaBudget.Core.Services
{
    public abstract class BudgetServiceCrudBase<TDataModel, TCoreModel> : BudgetServiceBase
        where TDataModel : BudgetDataBase
        where TCoreModel : BudgetCoreBase
    {
        protected DbSet<TDataModel> _dbSet;

        protected BudgetServiceCrudBase(LigovkaBudgetContext context, IMapper mapper)
            : base(context, mapper)
        {
            _dbSet = _context.Set<TDataModel>();
        }

        protected async Task<IEnumerable<TCoreModel>> GetAll(params string[] includeFields)
        {
            IEnumerable<TDataModel> dataModels = await DbSetWithInclude(includeFields).AsNoTracking()
                .ToArrayAsync();

            var coreModels = _mapper.Map<IEnumerable<TCoreModel>>(dataModels);
            return coreModels;
        }
        protected async Task<TCoreModel> FindOne(Expression<Func<TDataModel, bool>> predicate, params string[] includeFields)
        {
            TDataModel dataModel = await DbSetWithInclude(includeFields).AsNoTracking()
                .FirstOrDefaultAsync(predicate);

            var coreModel = _mapper.Map<TCoreModel>(dataModel);
            return coreModel;
        }
        protected async Task<IEnumerable<TCoreModel>> FindMany(Expression<Func<TDataModel, bool>> predicate, params string[] includeFields)
        {
            IEnumerable<TDataModel> dataModels = await DbSetWithInclude(includeFields).AsNoTracking()
                .Where(predicate)
                .ToArrayAsync();

            var coreModels = _mapper.Map<IEnumerable<TCoreModel>>(dataModels);
            return coreModels;
        }
        protected async Task<TCoreModel> AddOne(TCoreModel coreModel, params string[] includeFields)
        {
            var dataModel = _mapper.Map<TDataModel>(coreModel);

            await _dbSet.AddAsync(dataModel);

            await _context.SaveChangesAsync();
            await LoadReferences(dataModel, includeFields);

            var addedCoreModel = _mapper.Map<TCoreModel>(dataModel);
            return addedCoreModel;
        }
        protected async Task<IEnumerable<TCoreModel>> AddMany(IEnumerable<TCoreModel> coreModels, params string[] includeFields)
        {
            var dataModels = _mapper.Map<IEnumerable<TDataModel>>(coreModels);

            await _dbSet.AddRangeAsync(dataModels);

            await _context.SaveChangesAsync();

            foreach (var dataModel in dataModels)
                await LoadReferences(dataModel, includeFields);

            var addedCoreModels = _mapper.Map<IEnumerable<TCoreModel>>(dataModels);
            return addedCoreModels;
        }
        protected async Task<TCoreModel> UpdateOne(Expression<Func<TDataModel, bool>> predicate, Action<TDataModel> updater, params string[] includeFields)
        {
            var dataModel = await _dbSet.FirstOrDefaultAsync(predicate);

            updater(dataModel);

            _dbSet.Update(dataModel);
            await _context.SaveChangesAsync();
            await LoadReferences(dataModel, includeFields);

            var updatedCoreModel = _mapper.Map<TCoreModel>(dataModel);
            return updatedCoreModel;
        }
        protected async Task<bool> Remove(Expression<Func<TDataModel, bool>> predicate)
        {
            var dataModel = await _dbSet.FirstOrDefaultAsync(predicate);

            if (dataModel == null)
                return false;

            _dbSet.Remove(dataModel);
            await _context.SaveChangesAsync();

            return true;
        }

        protected IQueryable<TDataModel> DbSetWithInclude(params string[] includeFields)
        {
            if (includeFields == null || !includeFields.Any())
                return _dbSet;

            IQueryable<TDataModel> set = _dbSet;
            foreach (string includeField in includeFields)
                set = set.Include(includeField);

            return set;
        }
        protected async Task LoadReferences(TDataModel dataModel, params string[] includeFields)
        {
            if (includeFields == null || !includeFields.Any())
                return;

            foreach (string includeField in includeFields)
                await _context.Entry(dataModel).Reference(includeField).LoadAsync();
        }
        protected async Task LoadCollections(TDataModel dataModel, params string[] includeFields)
        {
            if (includeFields == null || !includeFields.Any())
                return;

            foreach (string includeField in includeFields)
                await _context.Entry(dataModel).Collection(includeField).LoadAsync();
        }
    }
}
