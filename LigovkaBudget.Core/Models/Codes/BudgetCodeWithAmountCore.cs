﻿namespace LigovkaBudget.Core.Models.Codes
{
    public class BudgetCodeWithAmountCore : BudgetCoreBase
    {
        public BudgetCodeCore Code { get; set; }
        public decimal? Amount { get; set; }
    }
}
