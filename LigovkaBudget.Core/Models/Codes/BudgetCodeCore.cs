﻿using LigovkaBudget.Infrastructure.Values;
using System;

namespace LigovkaBudget.Core.Models.Codes
{
    public class BudgetCodeCore : BudgetCoreBase
    {
        public int Id { get; set; }
        public int MunicipalityId { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public CodeType Type { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
