﻿using LigovkaBudget.Infrastructure.Values;

namespace LigovkaBudget.Core.Models.Codes
{
    public class BudgetCodeAddCore : BudgetCoreBase
    {
        public int MunicipalityId { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public CodeType Type { get; set; }
        public bool IsActive { get; set; }
    }
}
