﻿namespace LigovkaBudget.Core.Models.Codes
{
    public class BudgetCodeUpdateCore : BudgetCoreBase
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
