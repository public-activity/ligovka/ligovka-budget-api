﻿namespace LigovkaBudget.Core.Models.Views
{
    public class BudgetViewItemRelationCore
    {
        public int ChildAliasId { get; set; }
        public int ParentAliasId { get; set; }
        public bool IsPrimary { get; set; }
    }
}
