﻿using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Infrastructure.Values;
using System.Collections.Generic;

namespace LigovkaBudget.Core.Models.Views
{
    public class BudgetViewItemCore
    {
        public int AliasId { get; set; }
        public string AliasName { get; set; }
        public BudgetCodeCore Code { get; set; }
        public CodeType CodeType { get; set; }
        public bool IsDisplayed { get; set; }
        public decimal? CodeAmount { get; set; }
        public decimal? ChildrenAmount { get; set; }
        public IEnumerable<BudgetViewItemRelationCore> ParentsRelations { get; set; }
        public IEnumerable<BudgetViewItemRelationCore> ChildrenRelations { get; set; }
    }
}
