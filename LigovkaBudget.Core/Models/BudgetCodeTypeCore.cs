﻿using LigovkaBudget.Infrastructure.Values;

namespace LigovkaBudget.Core.Models
{
    public class BudgetCodeTypeCore : BudgetCoreBase
    {
        public int Id { get; set; }
        public CodeType Type { get; set; }
        public string Name { get; set; }
    }
}
