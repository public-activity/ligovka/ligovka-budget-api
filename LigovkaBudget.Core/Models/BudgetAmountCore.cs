﻿using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Core.Models.Details;

namespace LigovkaBudget.Core.Models
{
    public class BudgetAmountCore : BudgetCoreBase
    {
        public BudgetDetailsCore Details { get; set; }
        public BudgetCodeCore Code { get; set; }
        public decimal Value { get; set; }
    }
}
