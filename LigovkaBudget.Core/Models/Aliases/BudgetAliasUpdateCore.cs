﻿using LigovkaBudget.Core.Models.Codes;
using System.Collections.Generic;

namespace LigovkaBudget.Core.Models.Aliases
{
    public class BudgetAliasUpdateCore : BudgetCoreBase
    {
        public int Id { get; set; }
        public BudgetCodeCore Code { get; set; }
        public string Name { get; set; }
        public bool IsDisplayed { get; set; }
        public IEnumerable<BudgetAliasRelationCore> Relations { get; set; }
    }
}
