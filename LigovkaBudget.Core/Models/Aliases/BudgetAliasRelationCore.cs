﻿namespace LigovkaBudget.Core.Models.Aliases
{
    public class BudgetAliasRelationCore : BudgetCoreBase
    {
        public BudgetAliasCore ChildAlias { get; set; }
        public BudgetAliasCore ParentAlias { get; set; }
        public bool IsPrimary { get; set; }
    }
}
