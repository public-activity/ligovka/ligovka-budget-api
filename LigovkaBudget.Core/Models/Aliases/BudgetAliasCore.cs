﻿using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Core.Models.Presentations;
using LigovkaBudget.Infrastructure.Values;
using System.Collections.Generic;

namespace LigovkaBudget.Core.Models.Aliases
{
    public class BudgetAliasCore : BudgetCoreBase
    {
        public int Id { get; set; }
        public BudgetPresentationCore Presentation { get; set; }
        public BudgetCodeCore Code { get; set; }
        public CodeType CodeType { get; set; }
        public string Name { get; set; }
        public bool IsDisplayed { get; set; }
        public IEnumerable<BudgetAliasRelationCore> ParentsRelations { get; set; }
        public IEnumerable<BudgetAliasRelationCore> ChildrenRelations { get; set; }
    }
}
