﻿using LigovkaBudget.Infrastructure.Values;

namespace LigovkaBudget.Core.Models
{
    public class BudgetTypeCore : BudgetCoreBase
    {
        public int Id { get; set; }
        public BudgetType Type { get; set; }
        public string Name { get; set; }
    }
}
