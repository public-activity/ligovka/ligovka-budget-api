﻿using LigovkaBudget.Infrastructure.Values;

namespace LigovkaBudget.Core.Models
{
    public class BudgetViewTypeCore : BudgetCoreBase
    {
        public int Id { get; set; }
        public ViewType Type { get; set; }
        public string Name { get; set; }
    }
}
