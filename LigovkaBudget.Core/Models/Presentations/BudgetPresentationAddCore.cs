﻿namespace LigovkaBudget.Core.Models.Presentations
{
    public class BudgetPresentationAddCore : BudgetCoreBase
    {
        public int MunicipalityId { get; set; }
        public string Name { get; set; }
    }
}
