﻿namespace LigovkaBudget.Core.Models.Presentations
{
    public class BudgetPresentationCore : BudgetCoreBase
    {
        public int Id { get; set; }
        public int MunicipalityId { get; set; }
        public string Name { get; set; }
    }
}
