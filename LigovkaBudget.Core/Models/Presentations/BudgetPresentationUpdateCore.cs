﻿namespace LigovkaBudget.Core.Models.Presentations
{
    public class BudgetPresentationUpdateCore : BudgetCoreBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
