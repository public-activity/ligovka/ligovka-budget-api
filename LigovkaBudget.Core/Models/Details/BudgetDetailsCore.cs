﻿using LigovkaBudget.Infrastructure.Values;
using System;

namespace LigovkaBudget.Core.Models.Details
{
    public class BudgetDetailsCore : BudgetCoreBase
    {
        public int Id { get; set; }
        public int MunicipalityId { get; set; }
        public BudgetType Type { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public DateTime Date { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
