﻿using LigovkaBudget.Infrastructure.Values;
using System;

namespace LigovkaBudget.Core.Models.Details
{
    public class BudgetDetailsAddCore : BudgetCoreBase
    {
        public int MunicipalityId { get; set; }
        public BudgetType Type { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public DateTime Date { get; set; }
    }
}
