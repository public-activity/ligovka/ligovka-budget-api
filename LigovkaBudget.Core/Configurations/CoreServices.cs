using LigovkaBudget.Core.Interfaces;
using LigovkaBudget.Core.Services;
using Microsoft.Extensions.DependencyInjection;

namespace LigovkaBudget.Core.Configurations
{
    public static class CoreServices
    {
        public static void AddCoreServices(IServiceCollection services)
        {
            services.AddScoped<IBudgetAliasesService, BudgetAliasesService>();
            services.AddScoped<IBudgetAmountsService, BudgetAmountsService>();
            services.AddScoped<IBudgetCodesService, BudgetCodesService>();
            services.AddScoped<IBudgetDetailsService, BudgetDetailsService>();
            services.AddScoped<IBudgetPresentationsService, BudgetPresentationsService>();
            services.AddScoped<IBudgetViewsService, BudgetViewsService>();
            services.AddScoped<IBudgetViewTypesService, BudgetViewTypesService>();
            services.AddScoped<IBudgetCodeTypesService, BudgetCodeTypesService>();
            services.AddScoped<IBudgetTypesService, BudgetTypesService>();
        }
    }
}
