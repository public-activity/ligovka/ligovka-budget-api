﻿using AutoMapper;
using LigovkaBudget.Core.Models;
using LigovkaBudget.Core.Models.Aliases;
using LigovkaBudget.Core.Models.Codes;
using LigovkaBudget.Core.Models.Details;
using LigovkaBudget.Core.Models.Presentations;
using LigovkaBudget.Core.Models.Views;
using LigovkaBudget.Data.Models;
using LigovkaBudget.Infrastructure.Values;

namespace LigovkaBudget.Core.Configurations
{
    public class CoreMapperProfile : Profile
    {
        public CoreMapperProfile()
        {
            #region BudgetAlias
            CreateMap<BudgetAliasData, BudgetAliasCore>()
                .ForMember(coreModel => coreModel.Presentation,
                    opt => opt.MapFrom(dataModel => (object)dataModel.Presentation ?? new BudgetPresentationCore { Id = dataModel.PresentationId }))
                .ForMember(coreModel => coreModel.Code,
                    opt => opt.MapFrom(dataModel => (object)dataModel.Code
                        ?? (dataModel.CodeId.HasValue ? new BudgetCodeCore { Id = dataModel.CodeId.Value } : null)))
                .MaxDepth(2);

            CreateMap<BudgetAliasCore, BudgetAliasData>()
                .ForMember(dataModel => dataModel.Presentation, opt => opt.Ignore())
                .ForMember(dataModel => dataModel.PresentationId, opt => opt.MapFrom(coreModel => coreModel.Presentation.Id))
                .ForMember(dataModel => dataModel.Code, opt => opt.Ignore())
                .ForMember(dataModel => dataModel.CodeId, opt => opt.MapFrom(coreModel => coreModel.Code != null ? (int?)coreModel.Code.Id : null));
            #endregion

            #region BudgetAliasRelation
            CreateMap<BudgetAliasRelationData, BudgetAliasRelationCore>()
                .ForMember(coreModel => coreModel.ChildAlias,
                    opt => opt.MapFrom(dataModel => (object)dataModel.ChildAlias ?? new BudgetAliasCore { Id = dataModel.ChildAliasId }))
                .ForMember(coreModel => coreModel.ParentAlias,
                    opt => opt.MapFrom(dataModel => (object)dataModel.ParentAlias ?? new BudgetAliasCore { Id = dataModel.ParentAliasId }));

            CreateMap<BudgetAliasRelationData, BudgetViewItemRelationCore>()
                .ForMember(coreModel => coreModel.ChildAliasId, opt => opt.MapFrom(dataModel => dataModel.ChildAliasId))
                .ForMember(coreModel => coreModel.ParentAliasId, opt => opt.MapFrom(dataModel => dataModel.ParentAliasId));

            CreateMap<BudgetAliasRelationCore, BudgetAliasRelationData>()
                .ForMember(dataModel => dataModel.ChildAlias, opt => opt.Ignore())
                .ForMember(dataModel => dataModel.ChildAliasId, opt => opt.MapFrom(coreModel => coreModel.ChildAlias.Id))
                .ForMember(dataModel => dataModel.ParentAlias, opt => opt.Ignore())
                .ForMember(dataModel => dataModel.ParentAliasId, opt => opt.MapFrom(coreModel => coreModel.ParentAlias.Id));
            #endregion

            #region BudgetAmount
            CreateMap<BudgetAmountData, BudgetAmountCore>()
                .ForMember(coreModel => coreModel.Details,
                    opt => opt.MapFrom(dataModel => (object)dataModel.Details ?? new BudgetDetailsCore { Id = dataModel.DetailsId }))
                .ForMember(coreModel => coreModel.Code,
                    opt => opt.MapFrom(dataModel => (object)dataModel.Code ?? new BudgetCodeCore { Id = dataModel.CodeId }));

            CreateMap<BudgetAmountCore, BudgetAmountData>()
                .ForMember(dataModel => dataModel.Details, opt => opt.Ignore())
                .ForMember(dataModel => dataModel.DetailsId, opt => opt.MapFrom(coreModel => coreModel.Details.Id))
                .ForMember(dataModel => dataModel.Code, opt => opt.Ignore())
                .ForMember(dataModel => dataModel.CodeId, opt => opt.MapFrom(coreModel => coreModel.Code.Id));
            #endregion

            #region BudgetCode
            CreateMap<BudgetCodeData, BudgetCodeCore>();

            CreateMap<BudgetCodeCore, BudgetCodeData>();
            #endregion

            #region BudgetDetails
            CreateMap<BudgetDetailsData, BudgetDetailsCore>();

            CreateMap<BudgetDetailsCore, BudgetDetailsData>();
            #endregion

            #region BudgetPresentation
            CreateMap<BudgetPresentationData, BudgetPresentationCore>();

            CreateMap<BudgetPresentationCore, BudgetPresentationData>();
            #endregion

            #region BudgetPresentationViewType
            CreateMap<BudgetPresentationViewTypeData, ViewType>()
                .ConvertUsing(dataModel => dataModel.ViewType);
            #endregion

            #region ViewType
            CreateMap<BudgetViewTypeData, BudgetViewTypeCore>();
            #endregion

            #region CodeType
            CreateMap<BudgetCodeTypeData, BudgetCodeTypeCore>();
            #endregion

            #region BudgetType
            CreateMap<BudgetTypeData, BudgetTypeCore>();
            #endregion
        }
    }
}
