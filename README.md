# LigovkaBudget WebApi deployment

* Create docker image

docker image build -f "LigovkaBudget.WebApi\Dockerfile" --force-rm -t ligovka/budget/webapi:release .

or

docker image build -f "LigovkaBudget.WebApi\Dockerfile" --build-arg BUILD_CONFIGURATION=Debug --force-rm -t ligovka/budget/webapi:debug .

* Deploy docker container

docker container run -d -p 5000:80 -p 5001:443 --env "ConnectionStrings:LigovkaBudgetConnection"="$database_connection_string" --name ligovka-budget-webapi-release ligovka/budget/webapi:release

or

docker container run -d -p 5002:80 -p 5003:443 --env ASPNETCORE_ENVIRONMENT=Development --env "ConnectionStrings:LigovkaBudgetConnection"="$database_connection_string" --name ligovka-budget-webapi-debug ligovka/budget/webapi:debug