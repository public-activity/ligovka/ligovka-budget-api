﻿using LigovkaBudget.Data.Extensions;
using LigovkaBudget.Data.Models;
using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore;
using Npgsql;

namespace LigovkaBudget.Data
{
    public class LigovkaBudgetContext : DbContext
    {
        public DbSet<BudgetAliasData> Aliases { get; set; }
        public DbSet<BudgetAliasRelationData> AliasRelations { get; set; }
        public DbSet<BudgetAmountData> Amounts { get; set; }
        public DbSet<BudgetCodeData> Codes { get; set; }
        public DbSet<BudgetDetailsData> Details { get; set; }
        public DbSet<BudgetPresentationData> Presentations { get; set; }
        public DbSet<BudgetViewTypeData> ViewTypes { get; set; }
        public DbSet<BudgetCodeTypeData> CodeTypes { get; set; }
        public DbSet<BudgetTypeData> BudgetTypes { get; set; }
        public DbSet<BudgetPresentationViewTypeData> PresentationsViewTypes { get; set; }

        static LigovkaBudgetContext()
        {
            NpgsqlConnection.GlobalTypeMapper.MapEnum<BudgetType>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<CodeType>();
            NpgsqlConnection.GlobalTypeMapper.MapEnum<ViewType>();
        }
        public LigovkaBudgetContext(DbContextOptions options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Configure();
            builder.Seed();
        }
    }
}
