﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace LigovkaBudget.Data.Configurations
{
    public static class DataServices
    {
        public static void AddDataServices(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<LigovkaBudgetContext>(options => options.UseNpgsql(connectionString));
        }
    }
}
