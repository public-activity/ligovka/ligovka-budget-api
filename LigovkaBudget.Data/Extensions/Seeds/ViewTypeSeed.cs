﻿using LigovkaBudget.Data.Models;
using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore;

namespace LigovkaBudget.Data.Extensions.Seeds
{
    internal static class ViewTypeSeed
    {
        public static void SeedViewTypes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BudgetViewTypeData>()
                .HasData(
                    new BudgetViewTypeData { Id = (int)ViewType.Table, Type = ViewType.Table, Name = "Таблица" },
                    new BudgetViewTypeData { Id = (int)ViewType.Tree, Type = ViewType.Tree, Name = "Дерево" },
                    new BudgetViewTypeData { Id = (int)ViewType.Graph, Type = ViewType.Graph, Name = "График" }
                );
        }
    }
}
