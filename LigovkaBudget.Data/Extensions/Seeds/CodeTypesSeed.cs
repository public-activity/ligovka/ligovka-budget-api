﻿using LigovkaBudget.Data.Models;
using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore;

namespace LigovkaBudget.Data.Extensions.Seeds
{
    internal static class CodeTypesSeed
    {
        public static void SeedCodeTypes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BudgetCodeTypeData>()
                .HasData(
                    new BudgetCodeTypeData { Id = (int)CodeType.Income, Type = CodeType.Income, Name = "Доходы" },
                    new BudgetCodeTypeData { Id = (int)CodeType.Expenditure, Type = CodeType.Expenditure, Name = "Расходы" }
                );
        }
    }
}
