﻿using LigovkaBudget.Data.Models;
using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore;

namespace LigovkaBudget.Data.Extensions.Seeds
{
    internal static class BudgetTypesSeed
    {
        public static void SeedBudgetTypes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BudgetTypeData>()
                .HasData(
                    new BudgetTypeData { Id = (int)BudgetType.Project, Type = BudgetType.Project, Name = "Проект решения" },
                    new BudgetTypeData { Id = (int)BudgetType.Decision, Type = BudgetType.Decision, Name = "Решение" },
                    new BudgetTypeData { Id = (int)BudgetType.Utilization, Type = BudgetType.Utilization, Name = "Отчёт об исполнении" }
                );
        }
    }
}
