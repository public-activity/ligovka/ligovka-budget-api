﻿using LigovkaBudget.Data.Extensions.Seeds;
using LigovkaBudget.Data.Models;
using LigovkaBudget.Infrastructure.Constraints;
using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore;

namespace LigovkaBudget.Data.Extensions
{
    public static class ModelBuilderExtension
    {
        public static void Configure(this ModelBuilder builder)
        {
            builder.HasPostgresEnum<BudgetType>();
            builder.HasPostgresEnum<CodeType>();
            builder.HasPostgresEnum<ViewType>();

            builder.Entity<BudgetAliasData>(entity =>
            {
                entity.Property(alias => alias.Id)
                    .UseIdentityColumn();

                entity.HasOne(alias => alias.Presentation)
                    .WithMany()
                    .HasForeignKey(value => value.PresentationId);

                entity.HasOne(alias => alias.Code)
                    .WithMany()
                    .HasForeignKey(value => value.CodeId);

                entity.Property(alias => alias.Name)
                    .HasMaxLength(BudgetAliasConstraints.NameMaxLength)
                    .IsRequired();
            });

            builder.Entity<BudgetAliasRelationData>(entity =>
            {
                entity.HasKey(aliasRelation => new { aliasRelation.ChildAliasId, aliasRelation.ParentAliasId });

                entity.HasOne(aliasRelation => aliasRelation.ChildAlias)
                    .WithMany(alias => alias.ParentsRelations)
                    .HasForeignKey(aliasRelation => aliasRelation.ChildAliasId);

                entity.HasOne(aliasRelation => aliasRelation.ParentAlias)
                    .WithMany(alias => alias.ChildrenRelations)
                    .HasForeignKey(aliasRelation => aliasRelation.ParentAliasId);

                entity.Property(aliasRelation => aliasRelation.IsPrimary)
                    .HasDefaultValue(true)
                    .ValueGeneratedNever();
            });

            builder.Entity<BudgetAmountData>(entity =>
            {
                entity.HasOne(value => value.Details)
                    .WithMany()
                    .HasForeignKey(value => value.DetailsId);

                entity.HasOne(value => value.Code)
                    .WithMany()
                    .HasForeignKey(value => value.CodeId);

                entity.HasKey(value => new { value.DetailsId, value.CodeId });
            });

            builder.Entity<BudgetCodeData>(entity =>
            {
                entity.Property(code => code.Id)
                    .UseIdentityColumn();

                entity.Property(code => code.Number)
                    .HasMaxLength(DefaultConstraints.DefaultMaxLength)
                    .IsRequired();

                entity.HasIndex(code => new { code.MunicipalityId, code.Number })
                    .IsUnique();

                entity.Property(code => code.Name)
                    .HasMaxLength(BudgetCodeConstraints.NameMaxLength)
                    .IsRequired();

                entity.Property(code => code.Type)
                    .IsRequired()
                    .HasDefaultValue(CodeType.Income);

            });

            builder.Entity<BudgetDetailsData>(entity =>
            {
                entity.Property(details => details.Id)
                    .UseIdentityColumn();

                entity.Property(details => details.Name)
                    .HasMaxLength(DefaultConstraints.DefaultMaxLength)
                    .IsRequired();

                entity.Property(details => details.Date)
                    .HasColumnType("date");
            });

            builder.Entity<BudgetPresentationData>(entity =>
            {
                entity.Property(presentation => presentation.Id)
                    .UseIdentityColumn();

                entity.Property(presentation => presentation.Name)
                    .HasMaxLength(DefaultConstraints.DefaultMaxLength)
                    .IsRequired();

                entity.HasIndex(presentation => new { presentation.MunicipalityId, presentation.Name })
                    .IsUnique();
            });

            builder.Entity<BudgetViewTypeData>(entity =>
            {
                entity.Property(viewType => viewType.Id)
                    .UseIdentityColumn();

                entity.Property(viewType => viewType.Name)
                    .HasMaxLength(DefaultConstraints.DefaultMaxLength)
                    .IsRequired();

                entity.HasIndex(viewType => viewType.Name)
                    .IsUnique();
            });

            builder.Entity<BudgetCodeTypeData>(entity =>
            {
                entity.Property(codeType => codeType.Id)
                    .UseIdentityColumn();

                entity.Property(codeType => codeType.Name)
                    .HasMaxLength(DefaultConstraints.DefaultMaxLength)
                    .IsRequired();

                entity.HasIndex(codeType => codeType.Name)
                    .IsUnique();
            });

            builder.Entity<BudgetTypeData>(entity =>
            {
                entity.Property(budgetType => budgetType.Id)
                    .UseIdentityColumn();

                entity.Property(budgetType => budgetType.Name)
                    .HasMaxLength(DefaultConstraints.DefaultMaxLength)
                    .IsRequired();

                entity.HasIndex(budgetType => budgetType.Name)
                    .IsUnique();
            });

            builder.Entity<BudgetPresentationViewTypeData>(entity =>
            {
                entity.HasKey(presentationViewType => new { presentationViewType.PresentationId, presentationViewType.ViewType });

                entity.HasOne(presentationViewType => presentationViewType.Presentation)
                    .WithMany(presentation => presentation.СompatibleViewTypes)
                    .HasForeignKey(presentationViewType => presentationViewType.PresentationId);
            });
        }
        public static void Seed(this ModelBuilder builder)
        {
            builder.SeedViewTypes();
            builder.SeedCodeTypes();
            builder.SeedBudgetTypes();
        }
    }
}
