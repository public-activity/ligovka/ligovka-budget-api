﻿namespace LigovkaBudget.Data.Interfaces
{
    public interface IWithName
    {
        public string Name { get; set; }
    }
}
