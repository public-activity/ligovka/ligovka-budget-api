﻿namespace LigovkaBudget.Data.Interfaces
{
    public interface IWithId
    {
        public int Id { get; set; }
    }
}
