﻿using LigovkaBudget.Data.Interfaces;
using LigovkaBudget.Infrastructure.Values;

namespace LigovkaBudget.Data.Models
{
    public class BudgetTypeData : BudgetDataBase, IWithId, IWithName
    {
        public int Id { get; set; }
        public BudgetType Type { get; set; }
        public string Name { get; set; }
    }
}
