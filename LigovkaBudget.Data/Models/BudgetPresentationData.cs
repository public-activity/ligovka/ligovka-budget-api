﻿using System.Collections.Generic;

namespace LigovkaBudget.Data.Models
{
    public class BudgetPresentationData : BudgetDataBase
    {
        public int Id { get; set; }
        public int MunicipalityId { get; set; }
        public string Name { get; set; }
        public IEnumerable<BudgetPresentationViewTypeData> СompatibleViewTypes { get; set; }
    }
}
