﻿namespace LigovkaBudget.Data.Models
{
    public class BudgetAliasRelationData : BudgetDataBase
    {
        public int ParentAliasId { get; set; }
        public BudgetAliasData ParentAlias { get; set; }

        public int ChildAliasId { get; set; }
        public BudgetAliasData ChildAlias { get; set; }

        public bool IsPrimary { get; set; }
    }
}
