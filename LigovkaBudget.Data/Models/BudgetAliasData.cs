﻿using LigovkaBudget.Infrastructure.Values;
using System.Collections.Generic;

namespace LigovkaBudget.Data.Models
{
    public class BudgetAliasData : BudgetDataBase
    {
        public int Id { get; set; }

        public int PresentationId { get; set; }
        public BudgetPresentationData Presentation { get; set; }

        public int? CodeId { get; set; }
        public BudgetCodeData Code { get; set; }
        public CodeType CodeType { get; set; }

        public string Name { get; set; }
        public bool IsDisplayed { get; set; }

        public IEnumerable<BudgetAliasRelationData> ParentsRelations { get; set; }
        public IEnumerable<BudgetAliasRelationData> ChildrenRelations { get; set; }
    }
}
