﻿using LigovkaBudget.Data.Interfaces;
using LigovkaBudget.Infrastructure.Values;
using System;

namespace LigovkaBudget.Data.Models
{
    public class BudgetDetailsData : BudgetDataBase, IWithId, IWithName
    {
        public int Id { get; set; }
        public int MunicipalityId { get; set; }
        public BudgetType Type { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
        public DateTime Date { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
