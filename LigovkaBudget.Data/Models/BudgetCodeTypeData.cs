﻿using LigovkaBudget.Infrastructure.Values;

namespace LigovkaBudget.Data.Models
{
    public class BudgetCodeTypeData : BudgetDataBase
    {
        public int Id { get; set; }
        public CodeType Type { get; set; }
        public string Name { get; set; }
    }
}
