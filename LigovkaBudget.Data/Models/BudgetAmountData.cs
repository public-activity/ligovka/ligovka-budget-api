﻿namespace LigovkaBudget.Data.Models
{
    public class BudgetAmountData : BudgetDataBase
    {
        public int DetailsId { get; set; }
        public BudgetDetailsData Details { get; set; }

        public int CodeId { get; set; }
        public BudgetCodeData Code { get; set; }

        public decimal Value { get; set; }
    }
}
