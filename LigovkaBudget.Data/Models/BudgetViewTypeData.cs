﻿using LigovkaBudget.Infrastructure.Values;

namespace LigovkaBudget.Data.Models
{
    public class BudgetViewTypeData : BudgetDataBase
    {
        public int Id { get; set; }
        public ViewType Type { get; set; }
        public string Name { get; set; }
    }
}
