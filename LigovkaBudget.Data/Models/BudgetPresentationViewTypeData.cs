﻿using LigovkaBudget.Infrastructure.Values;

namespace LigovkaBudget.Data.Models
{
    public class BudgetPresentationViewTypeData : BudgetDataBase
    {
        public int PresentationId { get; set; }
        public BudgetPresentationData Presentation { get; set; }
        public ViewType ViewType { get; set; }
    }
}
