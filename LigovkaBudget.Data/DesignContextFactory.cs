﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace LigovkaBudget.Data
{
    //Need for Add-Migration command with LigovkaBudget.Data as startup project
    //Add-Migration Migration_Name -s LigovkaBudget.Data
    public class DesignContextFactory : IDesignTimeDbContextFactory<LigovkaBudgetContext>
    {
        public LigovkaBudgetContext CreateDbContext(string[] args)
        {
            //if (!System.Diagnostics.Debugger.IsAttached)
            //    System.Diagnostics.Debugger.Launch();

            IConfiguration configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(Directory.GetCurrentDirectory() + "/../LigovkaBudget.WebApi/appsettings.json")
                .Build();
            string connectionString = configuration.GetConnectionString("LigovkaBudgetConnection");

            var builder = new DbContextOptionsBuilder<LigovkaBudgetContext>();
            builder.UseNpgsql(connectionString);

            var context = new LigovkaBudgetContext(builder.Options);
            return context;
        }
    }
}
