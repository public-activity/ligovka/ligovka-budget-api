﻿using System;
using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LigovkaBudget.Data.Migrations
{
    public partial class Add_Code_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:budget_type", "project,decision,utilization")
                .Annotation("Npgsql:Enum:code_type", "income,expenditure")
                .OldAnnotation("Npgsql:Enum:budget_type", "project,decision,utilization");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Codes",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Codes",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<CodeType>(
                name: "Type",
                table: "Codes",
                type: "code_type",
                nullable: false,
                defaultValue: CodeType.Income);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "Codes",
                type: "timestamp without time zone",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Codes");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Codes");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "Codes");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Codes");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:budget_type", "project,decision,utilization")
                .OldAnnotation("Npgsql:Enum:budget_type", "project,decision,utilization")
                .OldAnnotation("Npgsql:Enum:code_type", "income,expenditure");
        }
    }
}
