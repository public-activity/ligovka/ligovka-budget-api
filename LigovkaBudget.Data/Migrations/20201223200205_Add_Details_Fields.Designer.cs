﻿// <auto-generated />
using System;
using LigovkaBudget.Data;
using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace LigovkaBudget.Data.Migrations
{
    [DbContext(typeof(LigovkaBudgetContext))]
    [Migration("20201223200205_Add_Details_Fields")]
    partial class Add_Details_Fields
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasPostgresEnum(null, "budget_type", new[] { "project", "decision", "utilization" })
                .HasPostgresEnum(null, "code_type", new[] { "income", "expenditure" })
                .UseIdentityByDefaultColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.1");

            modelBuilder.Entity("LigovkaBudget.Data.Models.BudgetAliasData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<int?>("CodeId")
                        .HasColumnType("integer");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255)
                        .HasColumnType("character varying(255)");

                    b.Property<int>("PresentationId")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("CodeId");

                    b.HasIndex("PresentationId");

                    b.ToTable("Aliases");
                });

            modelBuilder.Entity("LigovkaBudget.Data.Models.BudgetAliasRelationData", b =>
                {
                    b.Property<int>("AliasId")
                        .HasColumnType("integer");

                    b.Property<int>("ParentAliasId")
                        .HasColumnType("integer");

                    b.HasKey("AliasId", "ParentAliasId");

                    b.HasIndex("ParentAliasId");

                    b.ToTable("AliasRelations");
                });

            modelBuilder.Entity("LigovkaBudget.Data.Models.BudgetAmountData", b =>
                {
                    b.Property<int>("DetailsId")
                        .HasColumnType("integer");

                    b.Property<int>("CodeId")
                        .HasColumnType("integer");

                    b.Property<decimal>("Value")
                        .HasColumnType("numeric");

                    b.HasKey("DetailsId", "CodeId");

                    b.HasIndex("CodeId");

                    b.ToTable("Amounts");
                });

            modelBuilder.Entity("LigovkaBudget.Data.Models.BudgetCodeData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("timestamp without time zone");

                    b.Property<bool>("IsActive")
                        .HasColumnType("boolean");

                    b.Property<int>("MunicipalityId")
                        .HasColumnType("integer");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(1000)
                        .HasColumnType("character varying(1000)");

                    b.Property<string>("Number")
                        .IsRequired()
                        .HasMaxLength(255)
                        .HasColumnType("character varying(255)");

                    b.Property<CodeType>("Type")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("code_type")
                        .HasDefaultValue(CodeType.Income);

                    b.Property<DateTime?>("UpdatedAt")
                        .HasColumnType("timestamp without time zone");

                    b.HasKey("Id");

                    b.HasIndex("MunicipalityId", "Number")
                        .IsUnique();

                    b.ToTable("Codes");
                });

            modelBuilder.Entity("LigovkaBudget.Data.Models.BudgetDetailsData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("timestamp without time zone");

                    b.Property<DateTime>("Date")
                        .HasColumnType("date");

                    b.Property<int>("MunicipalityId")
                        .HasColumnType("integer");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255)
                        .HasColumnType("character varying(255)");

                    b.Property<BudgetType>("Type")
                        .HasColumnType("budget_type");

                    b.Property<DateTime?>("UpdatedAt")
                        .HasColumnType("timestamp without time zone");

                    b.Property<int>("Year")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.ToTable("Details");
                });

            modelBuilder.Entity("LigovkaBudget.Data.Models.BudgetPresentationData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .UseIdentityByDefaultColumn();

                    b.Property<int>("MunicipalityId")
                        .HasColumnType("integer");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(255)
                        .HasColumnType("character varying(255)");

                    b.HasKey("Id");

                    b.HasIndex("MunicipalityId", "Name")
                        .IsUnique();

                    b.ToTable("Presentations");
                });

            modelBuilder.Entity("LigovkaBudget.Data.Models.BudgetAliasData", b =>
                {
                    b.HasOne("LigovkaBudget.Data.Models.BudgetCodeData", "Code")
                        .WithMany()
                        .HasForeignKey("CodeId");

                    b.HasOne("LigovkaBudget.Data.Models.BudgetPresentationData", "Presentation")
                        .WithMany()
                        .HasForeignKey("PresentationId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Code");

                    b.Navigation("Presentation");
                });

            modelBuilder.Entity("LigovkaBudget.Data.Models.BudgetAliasRelationData", b =>
                {
                    b.HasOne("LigovkaBudget.Data.Models.BudgetAliasData", "Alias")
                        .WithMany()
                        .HasForeignKey("AliasId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("LigovkaBudget.Data.Models.BudgetAliasData", "ParentAlias")
                        .WithMany()
                        .HasForeignKey("ParentAliasId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Alias");

                    b.Navigation("ParentAlias");
                });

            modelBuilder.Entity("LigovkaBudget.Data.Models.BudgetAmountData", b =>
                {
                    b.HasOne("LigovkaBudget.Data.Models.BudgetCodeData", "Code")
                        .WithMany()
                        .HasForeignKey("CodeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("LigovkaBudget.Data.Models.BudgetDetailsData", "Details")
                        .WithMany()
                        .HasForeignKey("DetailsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Code");

                    b.Navigation("Details");
                });
#pragma warning restore 612, 618
        }
    }
}
