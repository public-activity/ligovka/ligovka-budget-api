﻿using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace LigovkaBudget.Data.Migrations
{
    public partial class Add_ViewTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:budget_type", "project,decision,utilization")
                .Annotation("Npgsql:Enum:code_type", "income,expenditure")
                .Annotation("Npgsql:Enum:view_type", "table,tree,graph")
                .OldAnnotation("Npgsql:Enum:budget_type", "project,decision,utilization")
                .OldAnnotation("Npgsql:Enum:code_type", "income,expenditure");

            migrationBuilder.CreateTable(
                name: "ViewTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Type = table.Column<ViewType>(type: "view_type", nullable: false),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ViewTypes", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "ViewTypes",
                columns: new[] { "Id", "Name", "Type" },
                values: new object[,]
                {
                    { 1, "Таблица", ViewType.Table },
                    { 2, "Дерево", ViewType.Tree },
                    { 3, "График", ViewType.Graph }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ViewTypes_Name",
                table: "ViewTypes",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ViewTypes");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:Enum:budget_type", "project,decision,utilization")
                .Annotation("Npgsql:Enum:code_type", "income,expenditure")
                .OldAnnotation("Npgsql:Enum:budget_type", "project,decision,utilization")
                .OldAnnotation("Npgsql:Enum:code_type", "income,expenditure")
                .OldAnnotation("Npgsql:Enum:view_type", "table,tree,graph");
        }
    }
}
