﻿using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LigovkaBudget.Data.Migrations
{
    public partial class Add_Alias_Code_Type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<CodeType>(
                name: "CodeType",
                table: "Aliases",
                type: "code_type",
                nullable: false,
                defaultValue: CodeType.Income);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CodeType",
                table: "Aliases");
        }
    }
}
