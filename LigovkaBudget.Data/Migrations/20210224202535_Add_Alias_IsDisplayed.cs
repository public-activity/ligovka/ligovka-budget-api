﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LigovkaBudget.Data.Migrations
{
    public partial class Add_Alias_IsDisplayed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDisplayed",
                table: "Aliases",
                type: "boolean",
                nullable: false,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDisplayed",
                table: "Aliases");
        }
    }
}
