﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LigovkaBudget.Data.Migrations
{
    public partial class Rename_Alias_Relation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AliasRelations_Aliases_AliasId",
                table: "AliasRelations");

            migrationBuilder.RenameColumn(
                name: "AliasId",
                table: "AliasRelations",
                newName: "ChildAliasId");

            migrationBuilder.AddForeignKey(
                name: "FK_AliasRelations_Aliases_ChildAliasId",
                table: "AliasRelations",
                column: "ChildAliasId",
                principalTable: "Aliases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AliasRelations_Aliases_ChildAliasId",
                table: "AliasRelations");

            migrationBuilder.RenameColumn(
                name: "ChildAliasId",
                table: "AliasRelations",
                newName: "AliasId");

            migrationBuilder.AddForeignKey(
                name: "FK_AliasRelations_Aliases_AliasId",
                table: "AliasRelations",
                column: "AliasId",
                principalTable: "Aliases",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
