﻿using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace LigovkaBudget.Data.Migrations
{
    public partial class Add_BudgetTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BudgetTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Type = table.Column<BudgetType>(type: "budget_type", nullable: false),
                    Name = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetTypes", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "BudgetTypes",
                columns: new[] { "Id", "Name", "Type" },
                values: new object[,]
                {
                    { 1, "Проект решения", BudgetType.Project },
                    { 2, "Решение", BudgetType.Decision },
                    { 3, "Отчёт об исполнении", BudgetType.Utilization }
                });

            migrationBuilder.CreateIndex(
                name: "IX_BudgetTypes_Name",
                table: "BudgetTypes",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BudgetTypes");
        }
    }
}
