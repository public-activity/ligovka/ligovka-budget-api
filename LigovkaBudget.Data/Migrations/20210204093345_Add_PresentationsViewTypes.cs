﻿using LigovkaBudget.Infrastructure.Values;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LigovkaBudget.Data.Migrations
{
    public partial class Add_PresentationsViewTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PresentationsViewTypes",
                columns: table => new
                {
                    PresentationId = table.Column<int>(type: "integer", nullable: false),
                    ViewType = table.Column<ViewType>(type: "view_type", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PresentationsViewTypes", x => new { x.PresentationId, x.ViewType });
                    table.ForeignKey(
                        name: "FK_PresentationsViewTypes_Presentations_PresentationId",
                        column: x => x.PresentationId,
                        principalTable: "Presentations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PresentationsViewTypes");
        }
    }
}
